﻿using Activator.Messages;
using Activator.Messages.Events;
using Intelligent.Shared.Testing.NUnit;
using MassTransit;
using NUnit.Framework;
using OrderTracking.Messages.OrderVerification;
using OrderVerification.Service.EventHandlers;
using OrderVerification.Service.Rules;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace OrderVerification.Service.Tests.EventHandling.ExecuteRulesHandling
{
    public class WhenRulesAreSuccessful : GivenWhenThen
    {
        private ExecuteRulesHandler<OrderActivated> _handler;

        private ConsumeContext<OrderActivated> _context;
        private OrderActivated _event;

        private IVerificationRule<OrderActivated> _rule;

        protected override void Given()
        {
            _event = new OrderActivated("COM-5es6fsd5sd", ActivationMethod.Direct, "For unit testing purposes", 88, "Zeno", new[] { "COM-5es6fsd5sd" }, DateTime.Now);
            
            _rule = MockRepository.GenerateMock<IVerificationRule<OrderActivated>>();
            _rule.Expect(x =>
                x.Verify(_event)).
                Return(Task.FromResult<IEnumerable<VerificationResult>>(new List<VerificationResult>() { VerificationResult.Succes(_event.OrderCode) })).
                Repeat.Once();

            var rules = new List<IVerificationRule<OrderActivated>>() { _rule };
            _handler = new ExecuteRulesHandler<OrderActivated>(rules);

            _context = MockRepository.GenerateMock<ConsumeContext<OrderActivated>>();
            _context.Expect(x => x.Message).Return(_event).Repeat.Once();
            _context.
                Expect(x => x.Publish(
                    Arg<OrderVerificationFailed>.Is.Anything,
                    Arg<CancellationToken>.Is.Anything)).
                Repeat.Never();
        }

        protected override void When()
        {
            var t = _handler.Consume(_context);
            t.Wait();
        }

        [Test]
        public void ShouldNotPublishFailedVerification()
        {
            _context.VerifyAllExpectations();
        }

        [Test]
        public void ShouldVerifyRules()
        {
            _rule.VerifyAllExpectations();
        }
    }
}