﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using Automatonymous;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Messaging.MassTransit3;
using Intelligent.Shared.Reflection;
using Intelligent.Shared.Services.Topshelf;
using MassTransit;
using MassTransit.EntityFrameworkIntegration;
using MassTransit.EntityFrameworkIntegration.Saga;
using MassTransit.Logging;
using MassTransit.Saga;
using MassTransit.SeriLogIntegration;
using Microsoft.Practices.Unity;
using OrderTracking.Service.StateTracking;
using Serilog;
using MassTransit.QuartzIntegration;
using OrderTracking.Service.StateTracking.Activities;
using Quartz;
using Quartz.Impl;

namespace OrderTracking.Service
{
    internal class ServiceFactory : UnityTopshelfServiceFactory
    {
        public override void ConfigureContainer(IUnityContainer container)
        {
            container.RegisterInstance(Log.Logger);
            container.RegisterType<ICommandHandler, CommandHandler>(new ContainerControlledLifetimeManager());
            container.RegisterType<IQueryHandler, QueryHandler>(new ContainerControlledLifetimeManager());

            var eventTypes = RegisterStateTrackingDependencies(container);
            RegisterBus(container, eventTypes);
        }

        private IEnumerable<Type> RegisterStateTrackingDependencies(IUnityContainer container)
        {
            SagaDbContextFactory sagaDbContextFactory =
                () => new SagaDbContext<OrderSaga, OrderSagaMap>(ConfigurationManager.ConnectionStrings["CombellGroup"].ConnectionString);

            container.RegisterType<ISagaRepository<OrderSaga>>(
               new ContainerControlledLifetimeManager(),
               new InjectionFactory(x => new EntityFrameworkSagaRepository<OrderSaga>(sagaDbContextFactory)));

            var scheduler = new StdSchedulerFactory().GetScheduler();
            container.RegisterInstance(scheduler);

            container.RegisterType<IOrderTrackingConfiguration, OrderTrackingConfiguration>(new ContainerControlledLifetimeManager());

            var eventTypes = new List<Type>();
            foreach (var type in TypeFinder.FindTypesWhichImplement(GetType().Assembly, typeof(IEventToActivityMapper<>)))
            {
                var implementedInterface = type.GetInterfaces().First();
                eventTypes.Add(implementedInterface.GetGenericArguments().First());
                container.RegisterType(implementedInterface, type, new ContainerControlledLifetimeManager());
            }

            var eventTracker = container.Resolve<OrderEventTracker>();

            foreach (var eventType in eventTypes)
            {
                var implementedInterface = typeof(IOrderEventTracker<>).MakeGenericType(eventType);
                container.RegisterInstance(implementedInterface, eventTracker, new ContainerControlledLifetimeManager());
            }

            return eventTypes;
        }

        private static void RegisterBus(IUnityContainer container, IEnumerable<Type> eventTypes)
        {
            var busControl = Bus.Factory.CreateUsingRabbitMq(x =>
            {
                Logger.UseLogger(new SerilogLogger());

                var host = x.Host(new Uri(ConfigurationManager.AppSettings["MassTransit_HostAddress"]), h =>
                {
                    h.Username(ConfigurationManager.AppSettings["MassTransit_UserName"]);
                    h.Password(ConfigurationManager.AppSettings["MassTransit_Password"]);
                });

                x.ReceiveEndpoint(host, "ordertracking.service", e =>
                {
                    e.PrefetchCount = 1;
                    e.Durable = true;
                    e.UseConcurrencyLimit(1);
                    e.StateMachineSaga(
                        container.Resolve<OrderTrackingStateMachine>(),
                        container.Resolve<ISagaRepository<OrderSaga>>());
                    e.Consumer<OrderVerificationFailedHandler>(container);
                    foreach (var eventType in eventTypes)
                    {
                        e.Consumer(typeof(OrderEventHandler<>).MakeGenericType(eventType), type => container.Resolve(type));
                    }
                });

                x.ReceiveEndpoint(host, "ordertracking.service.scheduler", e =>
                {
                    e.PrefetchCount = 1;
                    e.Durable = true;
                    e.UseConcurrencyLimit(1);
                    x.UseMessageScheduler(e.InputAddress);
                    e.Consumer(() => new ScheduleMessageConsumer(container.Resolve<IScheduler>()));
                    e.Consumer(() => new CancelScheduledMessageConsumer(container.Resolve<IScheduler>()));
                });
            });

            container.RegisterBusControl(new SyncPublishingBusControl(busControl));
        }
    }
}