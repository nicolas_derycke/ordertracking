using Intelligent.Shared.Services.Topshelf;
using MassTransit;
using MassTransit.QuartzIntegration;
using Microsoft.Practices.Unity;
using Quartz;

namespace OrderTracking.Service
{
    internal class Service : UnityTopshelfServiceBase
    {
        private readonly IBusControl _busControl;
        private readonly IScheduler _scheduler;

        public Service(IUnityContainer container, IBusControl busControl, IScheduler scheduler)
            : base(container)
        {
            _busControl = busControl;
            _scheduler = scheduler;
        }

        public override void Start()
        {
            _busControl.Start();
            _scheduler.JobFactory = new MassTransitJobFactory(_busControl);

            _scheduler.Start();
        }

        public override void Stop()
        {
            _scheduler.Standby();
            _busControl.Stop();

            _scheduler.Shutdown();
        }
    }
}