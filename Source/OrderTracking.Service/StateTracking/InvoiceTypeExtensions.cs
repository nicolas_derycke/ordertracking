﻿using Billing.Messages;

namespace OrderTracking.Service.StateTracking
{
    public static class InvoiceTypeExtensions
    {
        public static bool NeedsPayment(this InvoiceType invoiceType)
        {
            return invoiceType != InvoiceType.NewOrdersProforma &&
                   invoiceType != InvoiceType.PeriodFreeProforma &&
                   invoiceType != InvoiceType.PermanentFreeProforma;
        }
    }
}