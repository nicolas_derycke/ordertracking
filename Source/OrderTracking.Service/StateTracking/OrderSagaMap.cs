﻿using MassTransit.EntityFrameworkIntegration;

namespace OrderTracking.Service.StateTracking
{
    public class OrderSagaMap : SagaClassMapping<OrderSaga>
    {
        public OrderSagaMap()
        {
            ToTable("ordertracking.OrderSaga");

            Property(x => x.OrderCode);
            Property(x => x.CompositeStatus);
            Property(x => x.OrderStatus);
            Property(x => x.OrderNotCreatedId);
            Property(x => x.OrderManuallyActivatedId);
        }
    }
}