using System;
using System.Configuration;

namespace OrderTracking.Service.StateTracking
{
    public class OrderTrackingConfiguration : IOrderTrackingConfiguration
    {
        public OrderTrackingConfiguration()
        {
            int timeInMinutes;
            if (!int.TryParse(ConfigurationManager.AppSettings["MaxMinutesBetweenCheckOutAndCreation"], out timeInMinutes))
                timeInMinutes = 5;

            int monthsBetweenOrderInvoicedCustomerProFormaAndOrderStartedProcessing;
            if (!int.TryParse(ConfigurationManager.AppSettings["MaxMonthsBetweenOrderInvoicedCustomerProFormaAndOrderStartedProcessing"], out monthsBetweenOrderInvoicedCustomerProFormaAndOrderStartedProcessing))
                monthsBetweenOrderInvoicedCustomerProFormaAndOrderStartedProcessing = 2;


            MaxTimeBetweenCheckOutAndCreation = TimeSpan.FromMinutes(timeInMinutes);
            var timespanBetween = DateTime.Now.AddMonths(monthsBetweenOrderInvoicedCustomerProFormaAndOrderStartedProcessing).Subtract(
                DateTime.Now);
            MaxDaysBetweenOrderInvoicedCustomerProFormaAndOrderStartedProcessing = timespanBetween;

            MaxTimeBetweenManuallyActivatedAndOrderPaid = TimeSpan.FromDays(10);
        }

        public TimeSpan MaxTimeBetweenCheckOutAndCreation { get; private set; }
        public TimeSpan MaxDaysBetweenOrderInvoicedCustomerProFormaAndOrderStartedProcessing { get; private set; }
        public TimeSpan MaxTimeBetweenManuallyActivatedAndOrderPaid { get; private set; }
    }
}