namespace OrderTracking.Service.StateTracking
{
    public enum OrderInvoicingStatus
    {
        NotInvoiced,
        Invoiced
    }
}