using Billing.Messages;
using OrderTracking.Service.StateTracking.Activities;

namespace OrderTracking.Service.StateTracking
{
    public class OrderPaidMapper : IEventToActivityMapper<OrderPaid>
    {
        public Activity MapEventToActivity(OrderPaid @event)
        {
            return new Activity(@event.OrderCode, @event.PaidOn, "Order paid");
        }
    }
}