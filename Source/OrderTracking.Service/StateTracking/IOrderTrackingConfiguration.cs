using System;

namespace OrderTracking.Service.StateTracking
{
    public interface IOrderTrackingConfiguration
    {
        TimeSpan MaxTimeBetweenCheckOutAndCreation { get; }
        TimeSpan MaxDaysBetweenOrderInvoicedCustomerProFormaAndOrderStartedProcessing { get; }
        TimeSpan MaxTimeBetweenManuallyActivatedAndOrderPaid { get; }
    }
}