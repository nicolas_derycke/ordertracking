using OrderManagement.Messages.Events;

namespace OrderTracking.Service.StateTracking.Activities
{
    public class ProvisioningOrderStatusChangedMapper : IEventToActivityMapper<ProvisioningOrderStatusChanged>
    {
        public Activity MapEventToActivity(ProvisioningOrderStatusChanged @event)
        {
            if(string.IsNullOrEmpty(@event.Message))
                return new Activity(@event.OrderCode, @event.StatusChangedOn, string.Format("Provisioning order status changed to '{0}'", @event.Status));
            else
                return new Activity(@event.OrderCode, @event.StatusChangedOn, string.Format("Provisioning order status changed by '{0}' to status '{1}' with message '{2}'", @event.UpdatedBy, @event.Status, @event.Message));
        }
    }
}