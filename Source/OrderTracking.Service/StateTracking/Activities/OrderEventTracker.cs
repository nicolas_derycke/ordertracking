using Activator.Messages.Events;
using Billing.Messages;
using Intelligent.Shared.Core.Commands;
using Invoicing.Messages.Events;
using OrderManagement.Messages.Events;
using OrderTracking.Messages;
using OrderTracking.Service.StateTracking.Commands;
using Store.Messages.Events;
using System;

namespace OrderTracking.Service.StateTracking.Activities
{
    public class OrderEventTracker :
        IOrderEventTracker<CheckoutCompleted>,
        IOrderEventTracker<OrderCreated>,
        IOrderEventTracker<OrderActivated>,
        IOrderEventTracker<OrderActivationFailed>,
        IOrderEventTracker<OrderInvoiced>,
        IOrderEventTracker<ProvisioningOrderStatusChanged>,
        IOrderEventTracker<OrderCancelled>,
        IOrderEventTracker<OrderPaid>,
        IOrderEventTracker<OrderFinished>,
        IOrderEventTracker<OrderInvoiceTypeChangedEvent>
    {
        private readonly ICommandHandler _commandHandler;

        public OrderEventTracker(ICommandHandler commandHandler)
        {
            if (commandHandler == null) throw new ArgumentNullException("commandHandler");

            _commandHandler = commandHandler;
        }

        public void Handle(CheckoutCompleted @event)
        {
        }

        public void Handle(OrderCreated @event)
        {
        }

        public void Handle(OrderActivated @event)
        {
            _commandHandler.Execute(
                new UpdateActivationStateCommand(
                    @event.OrderCode,
                    OrderActivationStatus.Activated,
                    @event.ActivatedOn,
                    @event.Method,
                    @event.ActivationReason,
                    @event.ActivationById,
                    @event.ActivationBy));
        }

        public void Handle(OrderActivationFailed @event)
        {
            _commandHandler.Execute(new UpdateActivationStateCommand(@event.OrderCode, OrderActivationStatus.Failed, @event.ActivationFailedOn));
        }

        public void Handle(OrderInvoiced @event)
        {
            _commandHandler.Execute(
                new UpdateInvoicingStateCommand(
                    @event.OrderCode,
                    OrderInvoicingStatus.Invoiced,
                    @event.InvoiceId,
                    @event.InvoiceNumber,
                    InvoiceTypeMapper.MapToFriendlyName(@event.InvoiceType),
                    @event.InvoicedOn));
        }

        public void Handle(ProvisioningOrderStatusChanged @event)
        {
            _commandHandler.Execute(
                new UpdateProvisioningStateCommand(
                    @event.OrderCode,
                    @event.Status.MapToOrderProvisioningStatus(),
                    @event.StatusChangedOn));
        }

        public void Handle(OrderCancelled @event)
        {
        }

        public void Handle(OrderPaid @event)
        {
            _commandHandler.Execute(new UpdatePaymentStateCommand(@event.OrderCode, OrderPaymentStatus.Paid, @event.PaidOn));
        }
        
        public void Handle(OrderFinished @event)
        {
        }

        public void Handle(OrderInvoiceTypeChangedEvent @event)
        {
        }
    }
}