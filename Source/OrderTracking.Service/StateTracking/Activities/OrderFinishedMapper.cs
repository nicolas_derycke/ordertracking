using System;
using OrderTracking.Messages;

namespace OrderTracking.Service.StateTracking.Activities
{
    public class OrderFinishedMapper : IEventToActivityMapper<OrderFinished>
    {
        public Activity MapEventToActivity(OrderFinished @event)
        {
            return new Activity(@event.OrderCode, DateTime.Now, "Order Finished");
        }
    }
}