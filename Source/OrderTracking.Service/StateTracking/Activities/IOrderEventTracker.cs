﻿namespace OrderTracking.Service.StateTracking.Activities
{
    public interface IOrderEventTracker<TEvent> where TEvent : class
    {
        void Handle(TEvent @event);
    }
}