using System;
using System.Linq;
using Activator.Messages;
using Activator.Messages.Events;

namespace OrderTracking.Service.StateTracking.Activities
{
    public class OrderActivatedMapper : IEventToActivityMapper<OrderActivated>
    {
        public Activity MapEventToActivity(OrderActivated @event)
        {
            var message = string.Empty;
            switch (@event.Method)
            {
                case ActivationMethod.Direct:
                    message = "Order activated by direct activation";
                    break;
                case ActivationMethod.Manual:
                    message = string.Format("Order manually activated by '{0}' with the following reason: '{1}'", @event.ActivationBy, @event.ActivationReason);
                    break;
                case ActivationMethod.Payment:
                    message = "Order activated by payment";
                    break;
                case ActivationMethod.Free:
                    message = "Order activated because it's free";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (!@event.ProvisioningOrderCodes.Any())
                message += " (no provisioning needed)";

            return new Activity(@event.OrderCode, @event.ActivatedOn, message);
        }
    }
}