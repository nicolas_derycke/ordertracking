using Invoicing.Messages.Events;

namespace OrderTracking.Service.StateTracking.Activities
{
    public class OrderInvoiceTypeChangedMapper : IEventToActivityMapper<OrderInvoiceTypeChangedEvent>
    {
        public Activity MapEventToActivity(OrderInvoiceTypeChangedEvent @event)
        {
            return new Activity(@event.OrderCode, @event.OccuredOn, string.Format("Invoice type changed from '{0}' to '{1}'", @event.FromType, @event.ToType));
        }
    }
}