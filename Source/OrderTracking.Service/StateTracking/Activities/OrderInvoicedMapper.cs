using Billing.Messages;

namespace OrderTracking.Service.StateTracking.Activities
{
    public class OrderInvoicedMapper : IEventToActivityMapper<OrderInvoiced>
    {
        public Activity MapEventToActivity(OrderInvoiced @event)
        {
            var message = string.Format("Order invoiced on invoice of type '{0}' ", @event.InvoiceType);
            if (!string.IsNullOrWhiteSpace(@event.InvoiceNumber) && @event.InvoiceNumber != "0")
                message += string.Format("with invoice number '{0}'", @event.InvoiceNumber);
            else
                message += string.Format("with invoice id '{0}'", @event.InvoiceId);

            return new Activity(
                @event.OrderCode,
                @event.InvoicedOn,
                message);
        }
    }
}