namespace OrderTracking.Service.StateTracking.Activities
{
    public interface IEventToActivityMapper<TEvent>
    {
        Activity MapEventToActivity(TEvent @event);
    }
}