using System;

namespace OrderTracking.Service.StateTracking.Activities
{
    public class Activity
    {
        public DateTime OccuredOn { get; private set; }
        public string Message { get; private set; }

        public string OrderCode { get; set; }

        public Activity(string orderCode, DateTime occuredOn, string message)
        {
            OrderCode = orderCode;
            OccuredOn = occuredOn;
            Message = message;
        }
    }
}