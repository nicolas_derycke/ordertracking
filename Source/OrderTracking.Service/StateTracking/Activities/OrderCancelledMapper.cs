using Store.Messages.Events;

namespace OrderTracking.Service.StateTracking.Activities
{
    public class OrderCancelledMapper : IEventToActivityMapper<OrderCancelled>
    {
        public Activity MapEventToActivity(OrderCancelled @event)
        {
            return new Activity(@event.OrderCode, @event.CancelledOn, string.Format("Order cancelled by '{0}' with reason: {1}", @event.CancelledBy, @event.Reason));
        }
    }
}