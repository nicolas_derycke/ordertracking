﻿using System;
using System.Threading.Tasks;
using Intelligent.Shared.Core.Commands;
using MassTransit;
using OrderTracking.Messages.OrderVerification;
using OrderTracking.Service.StateTracking.Commands;

namespace OrderTracking.Service.StateTracking.Activities
{
    public class OrderVerificationFailedHandler : IConsumer<OrderVerificationFailed>
    {
        private readonly ICommandHandler _commandHandler;

        public OrderVerificationFailedHandler(ICommandHandler commandHandler)
        {
            if (commandHandler == null) throw new ArgumentNullException("commandHandler");

            _commandHandler = commandHandler;
        }

        public async Task Consume(ConsumeContext<OrderVerificationFailed> context)
        {
            var activity = MapToActivity(context.Message);
            _commandHandler.Execute(new CreateOrderActivityCommand(activity.OrderCode, activity.OccuredOn, activity.Message));
        }

        private static Activity MapToActivity(OrderVerificationFailed orderVerificationFailed)
        {
            var message = string.Format("Order verification failed for rule '{0}': {1}", orderVerificationFailed.Rule, orderVerificationFailed.Result);
            return new Activity(orderVerificationFailed.OrderCode, orderVerificationFailed.VerificationFailedOn, message);
        }
    }
}