﻿using System;
using System.Threading.Tasks;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using MassTransit;
using OrderTracking.Service.Queries;
using OrderTracking.Service.StateTracking.Commands;
using Serilog;

namespace OrderTracking.Service.StateTracking.Activities
{
    public class OrderEventHandler<TEvent> : IConsumer<TEvent> where TEvent : class
    {
        private readonly IEventToActivityMapper<TEvent> _eventToActivityMapper;
        private readonly IOrderEventTracker<TEvent> _orderEventTracker;
        private readonly IQueryHandler _queryHandler;
        private readonly ICommandHandler _commandHandler;
        private readonly ILogger _logger;

        public OrderEventHandler(
            IEventToActivityMapper<TEvent> eventToActivityMapper, 
            IOrderEventTracker<TEvent> orderEventTracker, 
            IQueryHandler queryHandler, 
            ICommandHandler commandHandler,
            ILogger logger)
        {
            if (eventToActivityMapper == null) throw new ArgumentNullException("eventToActivityMapper");
            if (orderEventTracker == null) throw new ArgumentNullException("orderEventTracker");
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (commandHandler == null) throw new ArgumentNullException("commandHandler");
            if (logger == null) throw new ArgumentNullException("logger");

            _eventToActivityMapper = eventToActivityMapper;
            _orderEventTracker = orderEventTracker;
            _queryHandler = queryHandler;
            _commandHandler = commandHandler;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<TEvent> context)
        {
            var activity = _eventToActivityMapper.MapEventToActivity(context.Message);
            var orderCode = new OrderCode(activity.OrderCode);
            if (orderCode.Type != OrderCodeType.Store)
                return;

            _logger.Information("Received event: {@event}", context.Message);

            if (!_queryHandler.Execute(new IsOrderStatePresentQuery(orderCode.Code)))
               _logger.Warning("No orderstate found for store order {@ordercode}", orderCode.Code);

            using (var transactionScope = TransactionScopeFactory.CreateReadCommitted())
            {
                _orderEventTracker.Handle(context.Message);
                _commandHandler.Execute(new CreateOrderActivityCommand(activity.OrderCode, activity.OccuredOn, activity.Message));

                transactionScope.Complete();
            }
        }
    }
}