using Activator.Messages.Events;

namespace OrderTracking.Service.StateTracking.Activities
{
    public class OrderActivationFailedMapper : IEventToActivityMapper<OrderActivationFailed>
    {
        public Activity MapEventToActivity(OrderActivationFailed @event)
        {
            return new Activity(@event.OrderCode, @event.ActivationFailedOn, "Order activation failed");
        }
    }
}