using Store.Messages.Events;

namespace OrderTracking.Service.StateTracking.Activities
{
    public class OrderCreatedMapper : IEventToActivityMapper<OrderCreated>
    {
        public Activity MapEventToActivity(OrderCreated @event)
        {
            return new Activity(
                @event.OrderCode,
                @event.CreatedOn,
                "Order created");
        }
    }
}