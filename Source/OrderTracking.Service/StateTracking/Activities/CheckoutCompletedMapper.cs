using Store.Messages.Events;

namespace OrderTracking.Service.StateTracking.Activities
{
    public class CheckoutCompletedMapper : IEventToActivityMapper<CheckoutCompleted>
    {
        public Activity MapEventToActivity(CheckoutCompleted @event)
        {
            return new Activity(
                @event.OrderCode,
                @event.CheckoutCompletedOn,
                string.Format(
                    "Checkout completed with checkout type '{0}' by {1} '{2}'",
                    @event.CheckoutType,
                    @event.IsCheckedoutByEmployee ? "employee" : "user",
                    @event.CheckedoutBy));
        }
    }
}