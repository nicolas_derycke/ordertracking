﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Activator.Messages;

namespace OrderTracking.Service.StateTracking
{
    public class OrderState
    {
        public ActivationMethod ActivationMethod { get; private set; }
        public OrderPaymentStatus PaymentStatus { get; private set; }

        public OrderState(ActivationMethod activationMethod, OrderPaymentStatus paymentStatus)
        {
            ActivationMethod = activationMethod;
            PaymentStatus = paymentStatus;
        }
    }
}
