using Activator.Messages.Events;
using Automatonymous;
using Billing.Messages;
using Intelligent.Shared.Core.Commands;
using Invoicing.Messages.Events;
using MassTransit;
using MassTransit.Internals.Extensions;
using OrderManagement.Messages.Events;
using OrderTracking.Messages;
using OrderTracking.Messages.OrderVerification;
using OrderTracking.Service.StateTracking.Commands;
using Store.Messages.Events;
using System;
using System.Linq;
using Activator.Messages;
using InvoiceType = Billing.Messages.InvoiceType;

namespace OrderTracking.Service.StateTracking
{
    public class OrderTrackingStateMachine : MassTransitStateMachine<OrderSaga>
    {
        public OrderTrackingStateMachine(ICommandHandler commandHandler, IOrderTrackingConfiguration orderTrackingConfiguration)
        {
            if (commandHandler == null) throw new ArgumentNullException("commandHandler");
            if (orderTrackingConfiguration == null) throw new ArgumentNullException("orderTrackingConfiguration");

            InstanceState(x => x.OrderStatus);

            Event(() => CheckoutCompleted,
                x =>
                {
                    x.CorrelateBy(order => order.OrderCode, ctx => ctx.Message.OrderCode);
                    x.InsertOnInitial = true;
                    x.SetSagaFactory(context => new OrderSaga
                    {
                        CorrelationId = NewId.NextGuid(),
                        OrderCode = context.Message.OrderCode,
                    });

                    x.SelectId(context => NewId.NextGuid());
                });

            Event(() => OrderCreated, x => x.CorrelateBy(order => order.OrderCode, ctx => ctx.Message.OrderCode));
            Event(() => OrderActivated, x => x.CorrelateBy(order => order.OrderCode, ctx => ctx.Message.OrderCode));
            Event(() => OrderActivationFailed, x => x.CorrelateBy(order => order.OrderCode, ctx => ctx.Message.OrderCode));
            Event(() => OrderInvoiced, x => x.CorrelateBy(order => order.OrderCode, ctx => ctx.Message.OrderCode));
            Event(() => OrderPaid, x => x.CorrelateBy(order => order.OrderCode, ctx => ctx.Message.OrderCode));
            Event(() => ProvisioningOrderStatusChanged, x => x.CorrelateBy(order => order.OrderCode, ctx => ctx.Message.OrderCode));
            Event(() => OrderCancelled, x => x.CorrelateBy(order => order.OrderCode, ctx => ctx.Message.OrderCode));
            Event(() => OrderInvoiceTypeChanged, x => x.CorrelateBy(order => order.OrderCode, ctx => ctx.Message.OrderCode));
            Event(() => ProvisioningFinished);
            Event(() => PaymentHandled);

            CompositeEvent(() => OrderFinished, state => state.CompositeStatus, OrderInvoiced, ProvisioningFinished, PaymentHandled);

            Schedule(() => OrderNotCreated, x => x.OrderNotCreatedId, x =>
            {
                x.Delay = orderTrackingConfiguration.MaxTimeBetweenCheckOutAndCreation;
                x.Received = configurator => configurator.CorrelateBy(saga => saga.OrderCode, context => context.Message.OrderCode);
            });

            Schedule(() => OrderManuallyActivated, x => x.OrderManuallyActivatedId, x =>
            {
                x.Delay = orderTrackingConfiguration.MaxTimeBetweenManuallyActivatedAndOrderPaid;
                x.Received = configurator => configurator.CorrelateBy(saga => saga.OrderCode, context => context.Message.OrderCode);
            });
            
            Initially(
                When(CheckoutCompleted)
                    .Then(context => commandHandler.Execute(
                        new CreateOrderStateCommand(
                            context.Instance.OrderCode,
                            context.Data.CheckoutCompletedOn,
                            context.Data.CheckedoutById,
                            context.Data.CheckedoutBy,
                            context.Data.IsCheckedoutByEmployee,
                            OrderActivationStatus.NotActivated,
                            OrderInvoicingStatus.NotInvoiced,
                            OrderPaymentStatus.NotPaid,
                            OrderProvisioningStatus.NotProvisioned)))
                    .Schedule(OrderNotCreated, context => new OrderNotCreated(context.Data.OrderCode))
                    .TransitionTo(CheckOutCompleted));

            During(CheckOutCompleted,
                When(OrderCreated)
                    .Unschedule(OrderNotCreated)
                    .TransitionTo(Created),
                When(OrderNotCreated.Received)
                    .Publish(context => new OrderVerificationFailed(
                        context.Instance.OrderCode,
                        VerificationRule.OrderShouldBeCreated,
                        string.Format("Order not created after {0}", orderTrackingConfiguration.MaxTimeBetweenCheckOutAndCreation.ToFriendlyString()),
                        DateTime.Now)));

            During(Created,
                When(OrderActivated)
                    .TransitionTo(Processing),
                When(OrderActivated, context => context.Data.Method == ActivationMethod.Manual)
                    .Schedule(OrderManuallyActivated, context => new OrderManuallyActivated(context.Data.OrderCode)),
                When(OrderActivationFailed)
                    .TransitionTo(Failed),
                
                When(OrderInvoiced, context => context.Data.InvoiceType != InvoiceType.NewCustomerOrderProforma)
                    .TransitionTo(Processing),
                When(OrderInvoiceTypeChanged, context =>
                    context.Data.FromType == Invoicing.Messages.Events.InvoiceType.NewCustomerOrderProforma &&
                    context.Data.ToType == Invoicing.Messages.Events.InvoiceType.Invoice)
                    .TransitionTo(Processing),
                When(OrderPaid)
                    .TransitionTo(Processing),
                When(OrderCancelled)
                    .TransitionTo(Cancelled));

            During(Processing,
                When(OrderActivationFailed)
                    .TransitionTo(Failed),
                When(ProvisioningOrderStatusChanged, context => context.Data.Status.IsErrorStatus())
                    .TransitionTo(Failed),
                When(OrderFinished)
                    .TransitionTo(Finished));

            During(Failed,
                When(OrderActivated)
                    .TransitionTo(Processing),
                When(ProvisioningOrderStatusChanged, context => !context.Data.Status.IsErrorStatus())
                    .TransitionTo(Processing));

            During(Processing, Failed,
                When(ProvisioningOrderStatusChanged, context => context.Data.Status.IsFinished())
                    .Then(context => context.Raise(ProvisioningFinished)),
                When(OrderActivated, context => !context.Data.ProvisioningOrderCodes.Any())
                    .Then(context => context.Raise(ProvisioningFinished)));

            DuringAny(
                When(OrderPaid)
                    .Then(context => context.Raise(PaymentHandled))
                    .Unschedule(OrderManuallyActivated),
                When(OrderInvoiced, context => !context.Data.InvoiceType.NeedsPayment())
                    .Then(context => context.Raise(PaymentHandled)),
                When(OrderManuallyActivated.Received)
                    .Publish(context => new ManuallyActivatedOrderUnpaid(context.Data.OrderCode)));

            WhenEnter(Processing, x => x.Publish(context => new OrderProcessingStarted(context.Instance.OrderCode)));
            WhenEnter(Failed, x => x.Publish(context => new OrderFailed(context.Instance.OrderCode)));
            WhenEnter(Finished, x => x.Publish(context => new OrderFinished(context.Instance.OrderCode)));


            OnUnhandledEvent(context => context.Ignore());
        }

        public State CheckOutCompleted { get; set; }
        public State Created { get; set; }
        public State Processing { get; set; }
        public State Failed { get; set; }
        public State Finished { get; set; }
        public State Cancelled { get; set; }

        public Event<CheckoutCompleted> CheckoutCompleted { get; set; }
        public Event<OrderCreated> OrderCreated { get; set; }

        public Event<OrderActivated> OrderActivated { get; set; }
        public Event<OrderActivationFailed> OrderActivationFailed { get; set; }

        public Event<OrderInvoiced> OrderInvoiced { get; set; }
        public Event<OrderPaid> OrderPaid { get; set; }
        public Event<OrderInvoiceTypeChangedEvent> OrderInvoiceTypeChanged { get; set; }

        public Event<ProvisioningOrderStatusChanged> ProvisioningOrderStatusChanged { get; set; }

        public Event<OrderCancelled> OrderCancelled { get; set; }

        public Event OrderFinished { get; set; }
        public Event ProvisioningFinished { get; set; }
        public Event PaymentHandled { get; set; }

        public Schedule<OrderSaga, OrderNotCreated> OrderNotCreated { get; set; }
        public Schedule<OrderSaga, OrderManuallyActivated> OrderManuallyActivated { get; set; }
    }
}