﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderTracking.Service.StateTracking
{
    public class OrderManuallyActivated
    {
        public string OrderCode { get; private set; }

        public OrderManuallyActivated(string orderCode)
        {
            OrderCode = orderCode;
        }
    }
}
