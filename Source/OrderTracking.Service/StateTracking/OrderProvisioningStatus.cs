namespace OrderTracking.Service.StateTracking
{
    public enum OrderProvisioningStatus
    {
        NotProvisioned,
        Processing,
        Pending,
        Errors,
        Finished,
        Cancelled,
        FailedValidation
    }
}