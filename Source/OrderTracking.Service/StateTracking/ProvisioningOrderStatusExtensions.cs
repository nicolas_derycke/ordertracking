﻿using System;
using System.Collections.Generic;
using OrderManagement.Messages;

namespace OrderTracking.Service.StateTracking
{
    public static class ProvisioningOrderStatusExtensions
    {
        private static readonly IDictionary<ProvisioningOrderStatusses, OrderProvisioningStatus>  StatusMap = new Dictionary<ProvisioningOrderStatusses, OrderProvisioningStatus>
        {
            {ProvisioningOrderStatusses.Canceled, OrderProvisioningStatus.Cancelled},
            {ProvisioningOrderStatusses.Canceled_Delete_Proforma, OrderProvisioningStatus.Cancelled},
            {ProvisioningOrderStatusses.Errors, OrderProvisioningStatus.Errors},
            {ProvisioningOrderStatusses.FailedValidation, OrderProvisioningStatus.FailedValidation},
            {ProvisioningOrderStatusses.Finished, OrderProvisioningStatus.Finished},
            {ProvisioningOrderStatusses.Initializing, OrderProvisioningStatus.Processing},
            {ProvisioningOrderStatusses.Unknown, OrderProvisioningStatus.Processing},
            {ProvisioningOrderStatusses.Pending,OrderProvisioningStatus.Pending},
            {ProvisioningOrderStatusses.Processing, OrderProvisioningStatus.Processing},
            {ProvisioningOrderStatusses.ReadyForProcessing, OrderProvisioningStatus.Processing}
        };

        public static OrderProvisioningStatus MapToOrderProvisioningStatus(this ProvisioningOrderStatusses provisioningOrderStatus)
        {
            OrderProvisioningStatus mappedStatus;
            if(!StatusMap.TryGetValue(provisioningOrderStatus, out mappedStatus))
                throw new ArgumentOutOfRangeException("provisioningOrderStatus", provisioningOrderStatus, "Invalid provisioning order status");

            return mappedStatus;
        }

        public static bool IsErrorStatus(this ProvisioningOrderStatusses provisioningOrderStatus)
        {
            return provisioningOrderStatus == ProvisioningOrderStatusses.Errors ||
                   provisioningOrderStatus == ProvisioningOrderStatusses.FailedValidation;
        }

        public static bool IsFinished(this ProvisioningOrderStatusses provisioningOrderStatus)
        {
            return provisioningOrderStatus == ProvisioningOrderStatusses.Finished ||
                   provisioningOrderStatus == ProvisioningOrderStatusses.Canceled ||
                   provisioningOrderStatus == ProvisioningOrderStatusses.Canceled_Delete_Proforma;
        }
    }
}