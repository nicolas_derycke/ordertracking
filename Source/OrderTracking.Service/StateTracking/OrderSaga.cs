﻿using Automatonymous;
using System;

namespace OrderTracking.Service.StateTracking
{
    public class OrderSaga : SagaStateMachineInstance
    {
        public Guid CorrelationId { get; set; }
        public string OrderCode { get; set; }
        public string OrderStatus { get; set; }
        public int CompositeStatus { get; set; }
        public Guid? OrderNotCreatedId { get; set; }
        public Guid? OrderManuallyActivatedId { get; set; }
    }
}