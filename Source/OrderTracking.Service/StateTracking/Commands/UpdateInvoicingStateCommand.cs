﻿using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using System;
using System.Data;

namespace OrderTracking.Service.StateTracking.Commands
{
    public class UpdateInvoicingStateCommand : ICommand
    {
        public string OrderCode { get; private set; }
        public OrderInvoicingStatus InvoicingStatus { get; private set; }
        public int InvoiceId { get; private set; }
        public string InvoiceNumber { get; private set; }
        public string InvoiceType { get; private set; }
        public DateTime InvoicingStatusLastUpdateDate { get; private set; }

        public UpdateInvoicingStateCommand(string orderCode, OrderInvoicingStatus invoicingStatus, int invoiceId, string invoiceNumber, string invoiceType, DateTime invoicingStatusLastUpdateDate)
        {
            OrderCode = orderCode;
            InvoicingStatus = invoicingStatus;
            InvoiceId = invoiceId;
            InvoiceNumber = invoiceNumber;
            InvoiceType = invoiceType;
            InvoicingStatusLastUpdateDate = invoicingStatusLastUpdateDate;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "ordertracking.UpdateInvoicingState",
                    new
                    {
                        OrderCode,
                        InvoicingStatus = InvoicingStatus.ToString(),
                        InvoiceId,
                        InvoiceNumber,
                        InvoiceType,
                        InvoicingStatusLastUpdateDate
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}