﻿using System;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;

namespace OrderTracking.Service.StateTracking.Commands
{
    public class UpdatePaymentStateCommand : ICommand
    {
        public string OrderCode { get; private set; }
        public OrderPaymentStatus PaymentStatus { get; private set; }
        public DateTime PaymentStatusLastUpdateDate { get; private set; }

        public UpdatePaymentStateCommand(string orderCode, OrderPaymentStatus paymentStatus, DateTime paymentStatusLastUpdateDate)
        {
            OrderCode = orderCode;
            PaymentStatus = paymentStatus;
            PaymentStatusLastUpdateDate = paymentStatusLastUpdateDate;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                connection.Execute("ordertracking.UpdatePaymentState",
                    new
                    {
                        OrderCode,
                        PaymentStatus = PaymentStatus.ToString(),
                        PaymentStatusLastUpdateDate
                    },
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}