﻿using System;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;

namespace OrderTracking.Service.StateTracking.Commands
{
    public class CreateOrderActivityCommand : ICommand
    {
        public string OrderCode { get; private set; }
        public DateTime OccuredOn { get; private set; }
        public string Message { get; private set; }

        public CreateOrderActivityCommand(string orderCode, DateTime occuredOn, string message)
        {
            OrderCode = orderCode;
            OccuredOn = occuredOn;
            Message = message;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute("ordertracking.CreateOrderActivity",
                    new
                    {
                        OrderCode,
                        OccuredOn,
                        Message
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}