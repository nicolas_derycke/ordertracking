﻿using System;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;

namespace OrderTracking.Service.StateTracking.Commands
{
    public class UpdateProvisioningStateCommand : ICommand
    {
        public string Ordercode { get; private set; }
        public OrderProvisioningStatus ProvisioningStatus { get; private set; }
        public DateTime ProvisioningStatusLastUpdateDate { get; private set; }

        public UpdateProvisioningStateCommand(string ordercode, OrderProvisioningStatus provisioningStatus, DateTime provisioningStatusLastUpdateDate)
        {
            Ordercode = ordercode;
            ProvisioningStatus = provisioningStatus;
            ProvisioningStatusLastUpdateDate = provisioningStatusLastUpdateDate;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute("ordertracking.UpdateProvisioningState",
                    new
                    {
                        Ordercode,
                        ProvisioningStatus = ProvisioningStatus.ToString(),
                        ProvisioningStatusLastUpdateDate
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}