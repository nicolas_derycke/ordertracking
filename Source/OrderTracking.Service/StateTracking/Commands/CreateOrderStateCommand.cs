﻿using System;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;

namespace OrderTracking.Service.StateTracking.Commands
{
    public class CreateOrderStateCommand : ICommand
    {
        public string OrderCode { get; private set; }
        public DateTime CreationDate { get; private set; }
        public int InitiatedById { get; private set; }
        public string InitiatedBy { get; private set; }
        public bool IsInitiatedByEmployee { get; private set; }
        public OrderActivationStatus ActivationStatus { get; private set; }
        public OrderInvoicingStatus InvoicingStatus { get; private set; }
        public OrderPaymentStatus PaymentStatus { get; private set; }
        public OrderProvisioningStatus ProvisioningStatus { get; private set; }

        public CreateOrderStateCommand(
            string orderCode, 
            DateTime creationDate,
            int initiatedById,
            string initiatedBy,
            bool isInitiatedByEmployee,
            OrderActivationStatus activationStatus, 
            OrderInvoicingStatus invoicingStatus, 
            OrderPaymentStatus paymentStatus, 
            OrderProvisioningStatus provisioningStatus)
        {
            OrderCode = orderCode;
            CreationDate = creationDate;
            InitiatedById = initiatedById;
            InitiatedBy = initiatedBy;
            IsInitiatedByEmployee = isInitiatedByEmployee;
            ActivationStatus = activationStatus;
            InvoicingStatus = invoicingStatus;
            PaymentStatus = paymentStatus;
            ProvisioningStatus = provisioningStatus;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "ordertracking.CreateOrderState",
                    new
                    {
                        OrderCode,
                        CreationDate,
                        InitiatedBy,
                        InitiatedById,
                        IsInitiatedByEmployee,
                        ActivationStatus = ActivationStatus.ToString(),
                        InvoicingStatus = InvoicingStatus.ToString(),
                        PaymentStatus = PaymentStatus.ToString(),
                        ProvisioningStatus = ProvisioningStatus.ToString()
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}