﻿using System;
using System.Data;
using Activator.Messages;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using OrderTracking.Service.StateTracking.Activities;

namespace OrderTracking.Service.StateTracking.Commands
{
    public class UpdateActivationStateCommand : ICommand
    {
        public string OrderCode { get; private set; }
        public OrderActivationStatus ActivationStatus { get; private set; }
        public DateTime ActivationStatusLastUpdateDate { get; private set; }
        public ActivationMethod? ActivationMethod { get; private set; }
        public string ActivationReason { get; private set; }
        public int? ActivationById { get; private set; }
        public string ActivationBy { get; private set; }

        public UpdateActivationStateCommand(string orderCode, OrderActivationStatus activationStatus, DateTime activationStatusLastUpdateDate, ActivationMethod activationMethod, string activationReason, int? activationById, string activationBy)
        {
            OrderCode = orderCode;
            ActivationStatus = activationStatus;
            ActivationStatusLastUpdateDate = activationStatusLastUpdateDate;
            ActivationMethod = activationMethod;
            ActivationReason = activationReason;
            ActivationById = activationById;
            ActivationBy = activationBy;
        }

        public UpdateActivationStateCommand(string orderCode, OrderActivationStatus activationStatus, DateTime activationStatusLastUpdateDate, ActivationMethod activationMethod)
            :this(orderCode, activationStatus, activationStatusLastUpdateDate, activationMethod, null, null, null)
        {
        }

        public UpdateActivationStateCommand(string orderCode, OrderActivationStatus activationStatus, DateTime activationStatusLastUpdateDate)
        {
            OrderCode = orderCode;
            ActivationStatus = activationStatus;
            ActivationStatusLastUpdateDate = activationStatusLastUpdateDate;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "ordertracking.UpdateActivationState",
                    new
                    {
                        OrderCode,
                        ActivationStatus = ActivationStatus.ToString(),
                        ActivationStatusLastUpdateDate,
                        ActivationMethod = ActivationMethod.ToString(),
                        ActivationReason,
                        ActivationBy,
                        ActivationById
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}