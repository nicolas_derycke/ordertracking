﻿using System.Collections.Generic;
using Billing.Messages;

namespace OrderTracking.Service.StateTracking
{
    public static class InvoiceTypeMapper
    {
        private static readonly IDictionary<InvoiceType, string> InvoiceTypeMap = new Dictionary<InvoiceType, string>
        {
            {InvoiceType.Invoice, "Regular invoice"},
            {InvoiceType.NewCustomerOrderProforma, "Customer proforma"},
            {InvoiceType.NewOrdersProforma, "Reseller proforma"},
            {InvoiceType.PeriodFreeProforma, "Period free"},
            {InvoiceType.PermanentFreeProforma, "Permanent free"}
        };

        public static string MapToFriendlyName(InvoiceType invoiceType)
        {
            string result;
            return !InvoiceTypeMap.TryGetValue(invoiceType, out result) ? string.Empty : result;
        }
    }
}