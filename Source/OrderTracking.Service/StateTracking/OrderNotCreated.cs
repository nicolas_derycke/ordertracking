namespace OrderTracking.Service.StateTracking
{
    public class OrderNotCreated
    {
        public string OrderCode { get; private set; }

        public OrderNotCreated(string orderCode)
        {
            OrderCode = orderCode;
        }
    }
}