namespace OrderTracking.Service.StateTracking
{
    public enum OrderActivationStatus
    {
        NotActivated,
        Activated,
        Failed
    }
}