namespace OrderTracking.Service.StateTracking
{
    public enum OrderPaymentStatus
    {
        NotPaid,
        Paid
    }
}