﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Activator.Messages;
using Billing.Messages;
using MassTransit;
using OrderTracking.Messages;
using OrderTracking.Service.Queries;
using OrderTracking.Service.StateTracking;
using Serilog;

namespace OrderTracking.Service.EventHandlers
{
    public class OrderExpiredEventHandler : IConsumer<OrderExpired>
    {
        private ILogger _logger;
        private IBus _bus;
        public OrderExpiredEventHandler(
            ILogger logger,
            IBus bus)
        {
            if(logger == null) throw new ArgumentNullException("logger");
            if(bus == null) throw new ArgumentNullException("bus");
            _logger = logger;
            _bus = bus;
        }
        public async Task Consume(ConsumeContext<OrderExpired> context)
        {
            var orderCode = context.Message.OrderCode;
            _logger.Information("OrderExpired received for ordercode: {0}", orderCode);

            var orderState = new GetOrderStateByOrderCodeQuery(orderCode).Execute();
            if (orderState.ActivationMethod == ActivationMethod.Manual &&
                orderState.PaymentStatus == OrderPaymentStatus.NotPaid)
                await _bus.Publish(new ManuallyActivatedOrderUnpaid(orderCode));
            else
                throw new Exception(string.Format("Order '{0}' is either not manually activated or already paid. ActicationMethod: {1}, PaymentStatus: {2}",
                    orderCode,
                    orderState.ActivationMethod,
                    orderState.PaymentStatus));

            _logger.Information("OrderExpired handled for ordercode: {0}", orderCode);
        }
    }
}
