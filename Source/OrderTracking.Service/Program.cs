﻿using Intelligent.Shared.Logging.Serilog;

namespace OrderTracking.Service
{
    class Program
    {
        static int Main()
        {
            LogConfiguration.Initialize();

            var factory = new ServiceFactory();
            return factory.Run<Service>();
        }
    }
}