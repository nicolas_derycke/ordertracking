﻿using System.Data;
using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;

namespace OrderTracking.Service.Queries
{
    public class IsOrderStatePresentQuery : IQuery<bool>
    {
        public string OrderCode { get; private set; }

        public IsOrderStatePresentQuery(string orderCode)
        {
            OrderCode = orderCode;
        }

        public bool Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return connection.ExecuteScalar<bool>(
                    "ordertracking.IsOrderStatePresent",
                    new
                    {
                        OrderCode
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}