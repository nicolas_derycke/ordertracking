﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Activator.Messages;
using Dapper;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using OrderTracking.Service.StateTracking;

namespace OrderTracking.Service.Queries
{
    public class GetOrderStateByOrderCodeQuery : IQuery<OrderState>
    {
        private readonly string _orderCode;
        public GetOrderStateByOrderCodeQuery(string orderCode)
        {
            _orderCode = orderCode;
        }
        public OrderState Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return connection.Query<dynamic>(
                    "ordertracking.GetOrderState",
                    new
                    {
                        OrderCode = _orderCode
                    },
                    commandType: CommandType.StoredProcedure)
                    .Select(o => new OrderState((ActivationMethod)o.ActivationMethod,(OrderPaymentStatus)o.PaymentStatus))
                    .FirstOrDefault();
        }
    }
}
