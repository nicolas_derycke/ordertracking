﻿namespace OrderTracking.Messages
{
    public class OrderProcessingStarted
    {
        public OrderProcessingStarted(string orderCode)
        {
            OrderCode = orderCode;
        }

        public string OrderCode { get; private set; }
    }
}