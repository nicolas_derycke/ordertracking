﻿namespace OrderTracking.Messages
{
    public class OrderFailed
    {
        public OrderFailed(string orderCode)
        {
            OrderCode = orderCode;
        }

        public string OrderCode { get; private set; }
    }
}