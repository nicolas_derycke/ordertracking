﻿using System;

namespace OrderTracking.Messages.OrderVerification
{
    public class OrderVerificationFailed
    {
        public string OrderCode { get; private set; }
        public VerificationRule Rule { get; private set; }
        public string Result { get; private set; }
        public DateTime VerificationFailedOn { get; private set; }

        public OrderVerificationFailed(string orderCode, VerificationRule rule, string result, DateTime verificationFailedOn)
        {
            this.OrderCode = orderCode;
            this.Rule = rule;
            this.Result = result;
            this.VerificationFailedOn = verificationFailedOn;
        }
    }
}