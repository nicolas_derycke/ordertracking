﻿namespace OrderTracking.Messages.OrderVerification
{
    public enum VerificationRule
    {
        OrderShouldBeProvisioned,
        OrderShouldBeCreated,
        OrderItemsShouldMatchBasketItems,
        OrderShouldBeProvisionedActions,
        IsInvoiceTypeAndBalanceConfiguredCorrect,
        InvoiceLinesShouldMatchOrderItems,
        InvoiceShouldMatchPriceInclVat,
        InvoicePdfShouldBeAvailable,
        ProductIsVisibleOnControlPanel,
        MailQueueShouldContainMail,
        TicketShouldBeCreated
    }
}