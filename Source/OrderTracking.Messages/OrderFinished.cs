﻿namespace OrderTracking.Messages
{
    public class OrderFinished
    {
        public OrderFinished(string orderCode)
        {
            OrderCode = orderCode;
        }

        public string OrderCode { get; private set; }
    }
}