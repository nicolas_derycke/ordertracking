﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderTracking.Messages
{
    public class ManuallyActivatedOrderUnpaid
    {
        public string OrderCode { get; private set; }

        public ManuallyActivatedOrderUnpaid(string orderCode)
        {
            OrderCode = orderCode;
        }
    }
}
