﻿using OrderTracking.Messages;

namespace OrderVerification.Service.Messages
{
    public class DelayedOrderFinishedVerification
    {
        public DelayedOrderFinishedVerification(string orderCode)
        {
            OrderCode = orderCode;
        }

        public string OrderCode { get; private set; }

    }
}