﻿using Intelligent.Shared.Services.Topshelf;
using MassTransit;
using Microsoft.Practices.Unity;

namespace OrderVerification.Service
{
    internal class Service : UnityTopshelfServiceBase
    {
        private readonly IBusControl _busControl;

        public Service(IUnityContainer container, IBusControl busControl)
            : base(container)
        {
            _busControl = busControl;
        }

        public override void Start()
        {
            _busControl.Start();
        }

        public override void Stop()
        {
            _busControl.Stop();
        }
    }
}