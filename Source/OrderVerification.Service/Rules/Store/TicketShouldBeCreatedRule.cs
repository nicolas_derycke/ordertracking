﻿using Core.Security;
using Intelligent.Core.Models;
using Intelligent.Shared.ServiceModel.Client;
using OrderTracking.Messages.OrderVerification;
using OrderVerification.Service.Proxy;
using Store.Messages.Events;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ticketing.API.Interface.Contracts.Enums;
using Ticketing.API.Interface.Contracts.Services;

namespace OrderVerification.Service.Rules.Store
{
    public class TicketShouldBeCreatedRule : IVerificationRule<OrderCreated>
    {
        private readonly ITokenProvider _tokenProvider;
        private readonly IServiceInvoker<ITicketingService> _ticketingServiceInvoker;

        public TicketShouldBeCreatedRule(ITokenProvider tokenProvider, IServiceInvoker<ITicketingService> ticketingServiceInvoker)
        {
            if (tokenProvider == null) throw new ArgumentNullException("tokenProvider");
            if (ticketingServiceInvoker == null) throw new ArgumentNullException("ticketingServiceInvoker");

            this._tokenProvider = tokenProvider;
            this._ticketingServiceInvoker = ticketingServiceInvoker;
        }

        public async Task<IEnumerable<VerificationResult>> Verify(OrderCreated message)
        {
            if (!OrderVerificationHelper.CheckIfOrderCodeTypeIsTypeOfStore(message.OrderCode))
                return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };

            var verificationResults = new List<VerificationResult>();
            var storeApiProxy = new StoreApiProxy(_tokenProvider);
            var order = await storeApiProxy.GetOrderContract(message.OrderCode);
            if (order.TicketId == 0 || string.IsNullOrWhiteSpace(order.TicketNumber))
                verificationResults.Add(CreateFailedVerificationResult(message, order.TicketNumber));

            var ticket = _ticketingServiceInvoker.Invoke(c => c.GetTicketByTicketNumber(order.TicketNumber));
            if (ticket == null || ticket.TicketID == 0)
                verificationResults.Add(CreateFailedVerificationResult(message, order.TicketNumber));

            var orderCustomerIdentifier = CustomerIdentifier.FromCustomerNumber(order.CustomerNumber);
            if(ticket.CustomerID != orderCustomerIdentifier.CustomerId)
                verificationResults.Add(CreateFailedVerificationResult(message, order.TicketNumber));

            if (!ticket.Title.Contains(message.OrderCode))
                verificationResults.Add(CreateFailedVerificationResult(message, order.TicketNumber));

            if ((TIC2_InternalStateEnum)ticket.InternalStateID != TIC2_InternalStateEnum.CLOSED)
                verificationResults.Add(CreateFailedVerificationResult(message, order.TicketNumber));

            if (verificationResults.Count == 0)
                verificationResults.Add(VerificationResult.Succes(message.OrderCode));

            return verificationResults;
        }

        private static VerificationResult CreateFailedVerificationResult(OrderCreated message, string ticketNumber)
        {
            return VerificationResult.Fail(
                message.OrderCode,
                VerificationRule.TicketShouldBeCreated,
                string.Format("Ticket '{0}' not found", ticketNumber));
        }
    }
}