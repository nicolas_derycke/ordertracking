﻿using Core.Security;
using OrderTracking.Messages.OrderVerification;
using OrderVerification.Service.Proxy;
using Serilog;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.Orders;
using Store.Messages.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderVerification.Service.Rules.Store
{
    public class OrderItemsShouldMatchBasketItemsRule : IVerificationRule<OrderCreated>
    {
        private ILogger _logger;
        private readonly ITokenProvider _tokenProvider;

        public OrderItemsShouldMatchBasketItemsRule(ILogger logger, ITokenProvider tokenProvider)
        {
            if (logger == null) throw new ArgumentNullException("logger");
            if (tokenProvider == null) throw new ArgumentNullException("tokenProvider");

            this._logger = logger;
            this._tokenProvider = tokenProvider;
        }

        public async Task<IEnumerable<VerificationResult>> Verify(OrderCreated message)
        {
            _logger.Information("Message received: {message}", message);

            if (!OrderVerificationHelper.CheckIfOrderCodeTypeIsTypeOfStore(message.OrderCode))
                return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };

            var storeApiProxy = new StoreApiProxy(_tokenProvider);
            var order = await storeApiProxy.GetOrderContract(message.OrderCode);
            var basket = await storeApiProxy.GetCheckedoutBasketContract(message.OrderCode);

            if (order.Items.Count() != basket.ItemCount)
                CreateFailedVerificationResult(message, "Order has a different item count than the basket");

            var verificationResults = new List<VerificationResult>();
            foreach (var basketItem in basket.Items)
            {
                var orderItemExistsOnBasket = order.Items.Any(i =>
                    i.ProductCode == basketItem.ProductCode &&
                    i.PromoProductCode == basketItem.PromoProductCode &&
                    i.ProductName == basketItem.ProductName &&
                    i.ProductType == basketItem.ProductType &&
                    i.Period == basketItem.Period &&
                    i.Quantity == basketItem.Quantity &&
                    i.Price.ExclVat == basketItem.Price.ExclVat &&
                    i.Price.InclVat == basketItem.Price.InclVat &&
                    i.Price.ReductionExclVat == basketItem.Price.ReductionExclVat &&
                    i.Price.ReductionInclVat == basketItem.Price.ReductionInclVat &&
                    i.TotalPrice.ExclVat == basketItem.TotalPrice.ExclVat &&
                    i.TotalPrice.InclVat == basketItem.TotalPrice.InclVat &&
                    i.TotalPrice.ReductionExclVat == basketItem.TotalPrice.ReductionExclVat &&
                    i.TotalPrice.ReductionInclVat == basketItem.TotalPrice.ReductionInclVat &&
                    i.IsRecurring == basketItem.IsRecurring &&
                    i.Attributes.Identifier == basketItem.Attributes.Identifier);

                if (!orderItemExistsOnBasket)
                {
                    var errorMessage = string.Format("The basket item '{0}' doesn't match with the order", basketItem.BasketItemId);
                    verificationResults.Add(CreateFailedVerificationResult(message, errorMessage));
                }
            }

            if (verificationResults.Where(r => r != null).Count() == 0)
                verificationResults.Add(VerificationResult.Succes(message.OrderCode));

            return verificationResults;

        }

        private static VerificationResult CreateFailedVerificationResult(OrderCreated message, string errorMessage)
        {
            return VerificationResult.Fail(
                message.OrderCode,
                VerificationRule.OrderItemsShouldMatchBasketItems,
                string.Format(
                    errorMessage,
                    message.OrderCode));
        }
    }
}