﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrderVerification.Service.Rules
{
    public interface IVerificationRule<T>
    {
        Task<IEnumerable<VerificationResult>> Verify(T message);
    }
}