﻿using Intelligent.Core.Models;
using Intelligent.Shared.Core;
using OrderTracking.Messages.OrderVerification;
using Store.API.Contracts.Orders;
using System.Linq;

namespace OrderVerification.Service.Rules
{
    public class OrderVerificationHelper
    {
        public static Optional<VerificationResult> CheckProvisioningOrder(OrderContract order, int? provisioningOrderId, string orderCode, VerificationRule verificationRule)
        {
            if (order.Items.All(i => i.ProductName == "Pre-registration"))
            {
                if (!provisioningOrderId.HasValue)
                    return VerificationResult.Succes(orderCode);

                return VerificationResult.Fail(
                    orderCode,
                    verificationRule,
                    "Order should not be provisioned");
            }

            if (!provisioningOrderId.HasValue)
                return VerificationResult.Fail(
                    orderCode,
                    verificationRule,
                    "Order should be provisioned");

            return Optional<VerificationResult>.Empty;
        }

        public static bool CheckIfOrderCodeTypeIsTypeOfStore(string orderCode)
        {
            return (new OrderCode(orderCode).Type.Equals(OrderCodeType.Store));
        }
    }
}