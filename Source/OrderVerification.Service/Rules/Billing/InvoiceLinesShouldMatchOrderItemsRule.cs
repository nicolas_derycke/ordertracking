﻿using Billing.API.Common.Interface.Contracts.Requests;
using Billing.API.Common.Interface.Contracts.Services;
using Billing.Messages;
using Core.Security;
using Intelligent.Shared.ServiceModel.Client;
using OrderTracking.Messages.OrderVerification;
using OrderVerification.Service.Proxy;
using Store.API.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderVerification.Service.Rules.Billing
{
    public class InvoiceLinesShouldMatchOrderItemsRule : IVerificationRule<OrderInvoiced>
    {
        private readonly ITokenProvider _tokenProvider;
        private readonly IServiceInvoker<IInvoiceService> _invoiceServiceInvoker;

        public InvoiceLinesShouldMatchOrderItemsRule(ITokenProvider tokenProvider, IServiceInvoker<IInvoiceService> invoiceServiceInvoker)
        {
            if (tokenProvider == null) throw new ArgumentNullException("tokenProvider");
            if (invoiceServiceInvoker == null) throw new ArgumentNullException("invoiceServiceInvoker");

            this._tokenProvider = tokenProvider;
            this._invoiceServiceInvoker = invoiceServiceInvoker;
        }

        public async Task<IEnumerable<VerificationResult>> Verify(OrderInvoiced message)
        {
            if (!OrderVerificationHelper.CheckIfOrderCodeTypeIsTypeOfStore(message.OrderCode))
                return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };

            var invoiceIds = new List<int>() { message.InvoiceId };
            var invoiceLines = _invoiceServiceInvoker.Invoke(c => c.GetInvoiceLines(new GetInvoiceLineListRequest() { InvoiceIDs = invoiceIds }));

            var storeApiProxy = new StoreApiProxy(_tokenProvider);
            var order = await storeApiProxy.GetOrderContract(message.OrderCode);
            var orderItems = order.Items.Where(i => !i.ParentId.HasValue && !i.Attributes.ContainsKey(AttributeKey.ManualActivation));

            if(message.InvoiceType == InvoiceType.PeriodFreeProforma)
                orderItems = orderItems.Where(i => i.TotalPrice.InclVat == 0 && i.TotalPrice.ReductionInclVat == 0);
            else
                orderItems = orderItems.Where(i => i.TotalPrice.InclVat > 0 || i.TotalPrice.ReductionInclVat > 0);

            var verificationResults = new List<VerificationResult>();
            foreach (var orderItem in orderItems)
            {
                var orderItemHasInvoiceLine = invoiceLines.Any(i =>
                    i.ProductID == orderItem.ProductId &&
                    i.ProductPeriod == orderItem.Period);
                if (!orderItemHasInvoiceLine)
                    verificationResults.Add(VerificationResult.Fail(
                        message.OrderCode,
                        VerificationRule.InvoiceLinesShouldMatchOrderItems,
                        string.Format(
                            "No invoice line found for product '{0}' in order item id '{1}'",
                            orderItem.ProductCode,
                            orderItem.OrderItemId)));
            }

            if (verificationResults.Where(r => r != null).Count() == 0)
                verificationResults.Add(VerificationResult.Succes(message.OrderCode));

            return verificationResults;
        }
    }
}