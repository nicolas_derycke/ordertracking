﻿using Billing.API.Common.Interface.Contracts.Requests;
using Billing.API.Common.Interface.Contracts.Services;
using Billing.API.Common.Interface.Contracts.Types;
using Billing.Messages;
using Core.Security;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.ServiceModel.Client;
using OrderTracking.Messages.OrderVerification;
using OrderVerification.Service.Proxy;
using OrderVerification.Service.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderVerification.Service.Rules.Billing
{
    public class InvoiceShouldMatchPriceInclVatRule : IVerificationRule<OrderInvoiced>
    {
        private readonly ITokenProvider _tokenProvider;
        private readonly IServiceInvoker<IInvoiceService> _invoiceServiceInvoker;
        private readonly IQueryHandler _queryHandler;

        public InvoiceShouldMatchPriceInclVatRule(ITokenProvider tokenProvider, IServiceInvoker<IInvoiceService> invoiceServiceInvoker, IQueryHandler queryHandler)
        {
            if (tokenProvider == null) throw new ArgumentNullException("tokenProvider");
            if (invoiceServiceInvoker == null) throw new ArgumentNullException("invoiceServiceInvoker");
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            this._tokenProvider = tokenProvider;
            this._invoiceServiceInvoker = invoiceServiceInvoker;
            this._queryHandler = queryHandler;
        }

        public async Task<IEnumerable<VerificationResult>> Verify(OrderInvoiced message)
        {
            if (!OrderVerificationHelper.CheckIfOrderCodeTypeIsTypeOfStore(message.OrderCode))
                return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };

            var storeApiProxy = new StoreApiProxy(_tokenProvider);
            var order = await storeApiProxy.GetOrderContract(message.OrderCode);
            var orderTotalInclVat = order.TotalPrice.InclVat;

            var invoiceDataContract = _invoiceServiceInvoker.Invoke(c => c.GetInvoice(new GetInvoiceDetailRequest() { InvoiceID = message.InvoiceId }));
            var invoiceTotalInclVat = invoiceDataContract.TotalWithTax;

            if (invoiceDataContract.InvoiceType == InvoiceTypeDataContract.NewOrdersProforma)
            {
                invoiceTotalInclVat = 0;
                var invoiceLineIds = await _queryHandler.ExecuteAsync(new GetInvoiceLineIdsByOrderCodeQuery(message.OrderCode));
                var invoiceLines = invoiceDataContract.InvoiceLines.Where(il => invoiceLineIds.Contains(il.InvoiceLineID));
                invoiceLines.ToList().ForEach(il => invoiceTotalInclVat += il.TotalWithoutTax + (il.TotalWithoutTax * (il.TaxPercentage/100)));

                invoiceTotalInclVat = Math.Round(invoiceTotalInclVat, 2, MidpointRounding.AwayFromZero);
            }

            

            if (orderTotalInclVat != invoiceTotalInclVat)
                return new List<VerificationResult>() { VerificationResult.Fail(
                    message.OrderCode,
                    VerificationRule.InvoiceShouldMatchPriceInclVat,
                    string.Format("The invoice total with vat '€{0}' doesn't match with the order '€{1}' for invoice id '{2}'", invoiceTotalInclVat, orderTotalInclVat, message.InvoiceId))};


            return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };
        }
    }
}