﻿using Billing.API.Common.Interface.Contracts.Requests;
using Billing.API.Common.Interface.Contracts.Services;
using Billing.Messages;
using Core.Security;
using Intelligent.Shared.ServiceModel.Client;
using OrderTracking.Messages.OrderVerification;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrderVerification.Service.Rules.Billing
{
    public class InvoicePdfShouldBeAvailableRule : IVerificationRule<OrderInvoiced>
    {
        private readonly ITokenProvider _tokenProvider;
        private readonly IServiceInvoker<IPDFServerService> _pdfServiceInvoker;

        public InvoicePdfShouldBeAvailableRule(ITokenProvider tokenProvider, IServiceInvoker<IPDFServerService> pdfServiceInvoker)
        {
            if (tokenProvider == null) throw new ArgumentNullException("tokenProvider");
            if (pdfServiceInvoker == null) throw new ArgumentNullException("pdfServiceInvoker");

            this._tokenProvider = tokenProvider;
            this._pdfServiceInvoker = pdfServiceInvoker;
        }

        public async Task<IEnumerable<VerificationResult>> Verify(OrderInvoiced message)
        {
            if (!OrderVerificationHelper.CheckIfOrderCodeTypeIsTypeOfStore(message.OrderCode))
                return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };

            var verificationResults = new List<VerificationResult>();
            var invoicePdf = _pdfServiceInvoker.Invoke(c => c.GetInvoicePDF(new GetInvoicePDFRequest(message.InvoiceId, false)));
            if (invoicePdf == null)
                return new List<VerificationResult>() { VerificationResult.Fail(
                    message.OrderCode,
                    VerificationRule.InvoicePdfShouldBeAvailable,
                    string.Format("No invoice pdf found for invoice id '{0}'", message.InvoiceId))};

            return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };
        }
    }
}