﻿using Billing.API.Common.Interface.Contracts.Requests;
using Billing.API.Common.Interface.Contracts.Services;
using Billing.API.Common.Interface.Contracts.Types;
using Billing.Messages;
using Core.Security;
using Intelligent.Shared.ServiceModel.Client;
using OrderTracking.Messages.OrderVerification;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrderVerification.Service.Rules.Payments
{
    public class IsInvoiceTypeAndBalanceConfiguredCorrectRule : IVerificationRule<OrderPaid>
    {
        private readonly ITokenProvider _tokenProvider;
        private readonly IServiceInvoker<IInvoiceService> _invoiceServiceInvoker;

        public IsInvoiceTypeAndBalanceConfiguredCorrectRule(ITokenProvider tokenProvider, IServiceInvoker<IInvoiceService> invoiceServiceInvoker)
        {
            if (tokenProvider == null) throw new ArgumentNullException("tokenProvider");
            if (invoiceServiceInvoker == null) throw new ArgumentNullException("invoiceServiceInvoker");

            this._tokenProvider = tokenProvider;
            this._invoiceServiceInvoker = invoiceServiceInvoker;
        }

        public async Task<IEnumerable<VerificationResult>> Verify(OrderPaid message)
        {
            if (!OrderVerificationHelper.CheckIfOrderCodeTypeIsTypeOfStore(message.OrderCode))
                return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };

            var invoiceDetailDataContract = _invoiceServiceInvoker.Invoke(c => c.GetInvoice(new GetInvoiceDetailRequest() { OrderCode = message.OrderCode }));

            // If no invoice was found, we asume it's a reseller proforma and which isn't the ordercode on the invoice.
            if(invoiceDetailDataContract == null || invoiceDetailDataContract.InvoiceID == 0)
                return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };

            var invoiceType = invoiceDetailDataContract.InvoiceType;
            if (invoiceType != InvoiceTypeDataContract.Invoice && invoiceType != InvoiceTypeDataContract.PeriodFreeProforma && invoiceType != InvoiceTypeDataContract.NewOrdersProforma)
                return new List<VerificationResult>() { VerificationResult.Fail(
                    message.OrderCode,
                    VerificationRule.IsInvoiceTypeAndBalanceConfiguredCorrect,
                    string.Format(
                        "No invoice with type 'Invoice', 'PeriodFreeProforma' or 'NewOrdersProforma' found. Invoice has type '{0}'",
                        invoiceType.ToString()))};

            if (invoiceDetailDataContract.Balance > 0)
                return new List<VerificationResult>() { VerificationResult.Fail(
                    message.OrderCode,
                    VerificationRule.IsInvoiceTypeAndBalanceConfiguredCorrect,
                        "The invoice balance is greater than zero.")};

            return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };
        }
    }
}