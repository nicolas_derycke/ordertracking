﻿using Combell.BusinessLogic.WebService.Contracts;
using Combell.Model.MijnCombell.UAC;
using Combell.Model.UAC.FaultContracts;
using Core.Security;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Net.Http;
using Intelligent.Shared.ServiceModel.Client;
using Linux.API.Contracts;
using Linux.API.Contracts.Responses;
using OrderTracking.Messages;
using OrderTracking.Messages.OrderVerification;
using OrderVerification.Service.Proxy;
using OrderVerification.Service.Queries;
using SiteBuilder.Queries.Model.Entities;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;

namespace OrderVerification.Service.Rules.OrderTracking
{
    public class ProductIsVisibleOnControlPanelRule : IVerificationRule<OrderFinished>
    {
        private readonly ITokenProvider _tokenProvider;
        private readonly IQueryHandler _queryHandler;

        private static HashSet<string> _productTypes = new HashSet<string>()
        {
            {"domainname"},
            {"basic-e-mail"},
            {"business-e-mail"},
            {"linux-shared-hosting"},
            {"windows-shared-hosting"},
            {"caching"},
            {"drupal"},
            {"joomla"},
            {"magento"},
            {"wordpress"},
            {"sitebuilder"},
            {"mysql-db"},
            {"ms-sql-db"},
            {"faxdiensten"},
            
            {"standard-domain-validation"},
            {"multi-domain-domain-validation"},
            {"wildcard-domain-validation"},
            {"standard-organisation"},
            {"multi-domain-organisation"},
            {"wildcard-organisation"},
            {"standard-ev"},
            {"multi-domain-ev"},

            {"uniek-ip-adres"}
        };

        public ProductIsVisibleOnControlPanelRule(ITokenProvider tokenProvider, IQueryHandler queryHandler)
        {
            if (tokenProvider == null) throw new ArgumentNullException("tokenProvider");
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            this._tokenProvider = tokenProvider;
            this._queryHandler = queryHandler;
        }

        public async Task<IEnumerable<VerificationResult>> Verify(OrderFinished message)
        {
            if (!OrderVerificationHelper.CheckIfOrderCodeTypeIsTypeOfStore(message.OrderCode))
                return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };

            var storeApiProxy = new StoreApiProxy(_tokenProvider);
            var order = await storeApiProxy.GetOrderContract(message.OrderCode);
            var orderState = await storeApiProxy.GetOrderState(message.OrderCode);

            if (orderState.ProvisioningState.Status == OrderProvisioningStatusContract.Cancelled)
                return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };

            if (order.Items.All(i => i.ProductName == "Pre-registration"))
                return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };

            var customerIdentifier = CustomerIdentifier.FromCustomerNumber(order.CustomerNumber);
            var userId = await _queryHandler.ExecuteAsync(new GetSuperUserQuery(customerIdentifier.CustomerId));
            var provider = new Provider(order.ProviderId);
            var info = new MCC_AccountInfo()
            {
                CustomerNumber = customerIdentifier.CustomerNumber,
                CustomerID = customerIdentifier.CustomerId,
                CompanyID = order.ProviderId,
                Identifier = "",
                LanguageID = 1,
                ResellerID = provider.ResellerId,
                UserID = userId
            };
            var criteria = new MCC_BaseSearchCriteria()
            {
                Page = 1,
                PageSize = 99999
            };

            var verificationResults = new List<VerificationResult>();
            foreach (var orderItem in order.Items.Where(i => !i.ParentId.HasValue && i.Attributes.Count() > 0 && !i.Attributes.ContainsKey(AttributeKey.ManualActivation)))
            {
                var productType = orderItem.ProductType.ToLower();
                if (!_productTypes.Contains(productType))
                    throw new Exception(string.Format("Product type '{0}' is not implemented for order item id '{1}'", productType, orderItem.OrderItemId));

                criteria.Sorting = new List<MCC_Sorting>();
                criteria.Filter = new List<MCC_SingleFilter>();

                DatabaseItemAttribute databaseItemAttribute;
                KeyValuePair<AttributeKey, IBasketItemAttribute> orderItemAttribute;
                var orderItemIdentifier = orderItem.Attributes.Identifier;
                info.Identifier = orderItemIdentifier;
                switch (productType)
                {
                    case "domainname":
                        if (orderItem.ProductName != "Pre-registration")
                        {
                            criteria.Filter.Add(new MCC_SingleFilter()
                            {
                                Key = "DomainName",
                                Value = orderItemIdentifier
                            });
                            using (var serviceInvoker = new ServiceInvoker<IBLWS_MijnCombellDomain>("MijnCombellDomain"))
                            {
                                var domainHostingDomains = serviceInvoker.Invoke(x => x.UAC_GetDomainHostingDomains(info, criteria));
                                if (!domainHostingDomains.IsSuccessfull)
                                    throw new Exception(string.Format("Couldn't get domainnames for order item id '{0}'", orderItem.OrderItemId));

                                if (!domainHostingDomains.Domains.Any(d => d.DomainName == orderItemIdentifier))
                                    verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                            }
                        }
                        break;
                    case "basic-e-mail":
                        MCC_BasicEmailOverview basicEmailDomain = null;
                        try
                        {
                            using (var serviceInvoker = new ServiceInvoker<IBLWS_MijnCombellBasicEmail>("MijnCombellBasicEmail"))
                                basicEmailDomain = serviceInvoker.Invoke(x => x.UAC_GetAccountByDomain(info));
                        }
                        catch (FaultException ex)
                        {
                            var validationFault = ex as FaultException<UAC_ValidationFault>;
                            if (validationFault.Detail.InternalMessage == "You have no permission to execute this request.")
                            {
                                verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                                break;
                            }

                            throw ex;
                        }

                        if (basicEmailDomain == null || basicEmailDomain.AllowedMailboxes == 0)
                            verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                        break;
                    case "business-e-mail":
                        MCC_HE_HostedExchangeOverview businessEmailDomain = null;
                        try
                        {
                            using (var serviceInvoker = new ServiceInvoker<IBLWS_MijnCombellHostedExchange>("MijnCombellHostedExchange"))
                                businessEmailDomain = serviceInvoker.Invoke(x => x.UAC_GetAccountByDomain(info));
                        }
                        catch (FaultException ex)
                        {
                            var validationFault = ex as FaultException<UAC_ValidationFault>;
                            if (validationFault.Detail.InternalMessage == "You have no permission to execute this request.")
                            {
                                verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                                break;
                            }

                            throw ex;
                        }

                        if (businessEmailDomain == null || businessEmailDomain.AllowedMailboxes == 0)
                            verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));

                        break;
                    case "linux-shared-hosting":
                    case "drupal":
                    case "joomla":
                    case "magento":
                    case "wordpress":
                        LHC_Account linuxAccount = null;
                        try
                        {
                            using (var invoker = new ServiceInvoker<IBLWS_MijnCombellLinuxHosting>("MijnCombellLinuxHosting"))
                                linuxAccount = invoker.Invoke(of => of.UAC_GetAccountByDomain(info));

                            if (linuxAccount == null || linuxAccount.Domain != orderItemIdentifier)
                                verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                        }
                        catch (FaultException ex)
                        {
                            var validationFault = ex as FaultException<UAC_ValidationFault>;
                            if (validationFault.Detail.InternalMessage == "You have no permission to execute this request.")
                            {
                                verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                                break;
                            }

                            throw ex;
                        }

                        break;
                    case "windows-shared-hosting":
                        HC_Account windowsAccount = null;
                        try
                        {
                            using (var invoker = new ServiceInvoker<IBLWS_MijnCombellWindowsHosting>("MijnCombellWindowsHosting"))
                                windowsAccount = invoker.Invoke(of => of.UAC_GetAccountByDomain(info));
                        }
                        catch (FaultException ex)
                        {
                            var validationFault = ex as FaultException<UAC_ValidationFault>;
                            if (validationFault.Detail.InternalMessage == "You have no permission to execute this request.")
                            {
                                verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                                break;
                            }

                            throw ex;
                        }

                        if (windowsAccount == null || windowsAccount.Domain != orderItemIdentifier)
                            verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));

                        break;
                    case "caching":
                        var cacheAddonsUrl = string.Format("{0}/api/{1}/cacheaddons", ConfigurationManager.AppSettings["LinuxServiceUrl"], orderItemIdentifier);
                        var cachingStronglyTypedHttpClientBuilder = new StronglyTypedHttpClientBuilder();
                        cachingStronglyTypedHttpClientBuilder.WithHeader("Intelli-UAC-UserId", userId);

                        IEnumerable<CacheAddon> cacheAddons = null;
                        using (var httpClient = cachingStronglyTypedHttpClientBuilder.Build())
                            cacheAddons = httpClient.GetAsync<IEnumerable<CacheAddon>>(cacheAddonsUrl).Result;

                        if (orderItem.ProductName.Contains(CacheAddonNames.Redis))
                        {
                            if (!cacheAddons.Any(ca => ca.Name == CacheAddonNames.Redis && ca.Status != CacheAddonStatus.NotOrdered))
                                verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                        }
                        else if (orderItem.ProductName.Contains(CacheAddonNames.Memcached))
                        {
                            if (!cacheAddons.Any(ca => ca.Name == CacheAddonNames.Memcached && ca.Status != CacheAddonStatus.NotOrdered))
                                verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                        }
                        else if (orderItem.ProductName.Contains(CacheAddonNames.Varnish))
                        {
                            if (!cacheAddons.Any(ca => ca.Name == CacheAddonNames.Varnish && ca.Status != CacheAddonStatus.NotOrdered))
                                verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                        }
                        else
                        {
                            throw new Exception(
                                string.Format(
                                    "Couldn't process the caching item '{0}' for order item id '{1}'",
                                    orderItem.ProductName,
                                    orderItem.OrderItemId));
                        }

                        break;
                    case "sitebuilder":
                        var sitebuilderUrl = string.Format(
                            "{0}/api/accounts?skip=0&take=1&filter=identifier eq '{1}'",
                            ConfigurationManager.AppSettings["SitebuilderApiUrl"],
                            orderItemIdentifier);
                        var sitebuilderStronglyTypedHttpClientBuilder = new StronglyTypedHttpClientBuilder();
                        sitebuilderStronglyTypedHttpClientBuilder.WithHeader("Intelli-UAC-UserId", userId);

                        IEnumerable<SiteBuilderProduct> sitebuilderProducts = null;
                        using (var httpClient = sitebuilderStronglyTypedHttpClientBuilder.Build())
                            sitebuilderProducts = await httpClient.GetAsync<IEnumerable<SiteBuilderProduct>>(new Uri(sitebuilderUrl));

                        if (sitebuilderProducts.Count() == 0)
                            verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));

                        var sitebuilderProduct = sitebuilderProducts.First();
                        if (!sitebuilderProduct.IsAccessible)
                            verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                        if (sitebuilderProduct.Identifier != orderItemIdentifier)
                            verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                        if (!sitebuilderProduct.Links.Any(l => l.Rel == "access"))
                            verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));

                        break;
                    case "mysql-db":
                        orderItemIdentifier = "";

                        orderItemAttribute = orderItem.Attributes.ToList().First(a => a.Key == AttributeKey.Database);
                        databaseItemAttribute = (DatabaseItemAttribute)orderItemAttribute.Value;

                        info.Identifier = info.CustomerNumber.ToString();
                        if (databaseItemAttribute.UpgradeFromDatabaseName != null)
                        {
                            criteria.Filter.Add(new MCC_SingleFilter() { Key = "DatabaseName", Value = databaseItemAttribute.UpgradeFromDatabaseName });
                            MCC_DatabaseResponse mySqlDatabaseResponse;
                            using (var invoker = new ServiceInvoker<IBLWS_MijnCombellMySQL>("MijnCombellMySQL"))
                                mySqlDatabaseResponse = invoker.Invoke(of => of.UAC_GetDatabases(info, criteria));

                            if (mySqlDatabaseResponse.Databases.Count == 0)
                                verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                        }
                        else
                        {
                            MCC_DatabaseCreditsResponse mySqlDatabaseCreditsResponse = null;
                            using (var invoker = new ServiceInvoker<IBLWS_MijnCombellMySQL>("MijnCombellMySQL"))
                                mySqlDatabaseCreditsResponse = invoker.Invoke(of => of.UAC_GetDatabaseCreditsForAccount(info, criteria));

                            if (!mySqlDatabaseCreditsResponse.Credits.Any(c => c.InUse == false && c.DatabaseType == "MySQL"))
                                verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                        }

                        break;
                    case "ms-sql-db":
                        orderItemIdentifier = "";

                        orderItemAttribute = orderItem.Attributes.ToList().First(a => a.Key == AttributeKey.Database);
                        databaseItemAttribute = (DatabaseItemAttribute)orderItemAttribute.Value;

                        info.Identifier = info.CustomerNumber.ToString();
                        if (databaseItemAttribute.UpgradeFromDatabaseName != null)
                        {
                            criteria.Filter.Add(new MCC_SingleFilter() { Key = "DatabaseName", Value = databaseItemAttribute.UpgradeFromDatabaseName });
                            MCC_DatabaseResponse msSqlDatabaseResponse;
                            using (var invoker = new ServiceInvoker<IBLWS_MijnCombellMsSqlServer>("MijnCombellMsSqlServer"))
                                msSqlDatabaseResponse = invoker.Invoke(of => of.UAC_GetDatabases(info, criteria));

                            if (msSqlDatabaseResponse.Databases.Count == 0)
                                verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                        }
                        else
                        {
                            MCC_DatabaseCreditsResponse msSqlServerDatabaseCreditsResponse = null;
                            using (var invoker = new ServiceInvoker<IBLWS_MijnCombellMsSqlServer>("MijnCombellMsSqlServer"))
                                msSqlServerDatabaseCreditsResponse = invoker.Invoke(of => of.UAC_GetDatabaseCreditsForAccount(info, criteria));

                            if (!msSqlServerDatabaseCreditsResponse.Credits.Any(c => c.InUse == false && c.DatabaseType == "MsSQL"))
                                verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                        }

                        break;
                    case "faxdiensten":
                        break; //ToDo: Implement
                    case "standard-domain-validation":
                    case "multi-domain-domain-validation":
                    case "wildcard-domain-validation":
                    case "standard-organisation":
                    case "multi-domain-organisation":
                    case "wildcard-organisation":
                    case "standard-ev":
                    case "multi-domain-ev":
                        MCC_SSLCertificateResponse sslCertificates;
                        using (var invoker = new ServiceInvoker<IBLWS_MijnCombellSSL>("MijnCombellSSL"))
                            sslCertificates = invoker.Invoke(of => of.UAC_GetSSLCertificates(info, criteria));

                        MCC_SSLCertificate sslCertificate = sslCertificates.SSLCertificates.FirstOrDefault(c => c.MainHostname == info.Identifier);
                        if (sslCertificate == null)
                            verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));

                        break;
                    case "uniek-ip-adres":
                        break;
                    default:
                        verificationResults.Add(CreateFailedVerificationResult(message, productType, orderItemIdentifier, orderItem.OrderItemId));
                        break;
                };
            }

            if (verificationResults.Where(r => r != null).Count() == 0)
                verificationResults.Add(VerificationResult.Succes(message.OrderCode));

            return verificationResults;
        }

        private static VerificationResult CreateFailedVerificationResult(OrderFinished message, string productType, string identifier, int orderItemId)
        {
            return VerificationResult.Fail(
                message.OrderCode,
                VerificationRule.ProductIsVisibleOnControlPanel,
                string.Format("Service doesn't return the product '{0}' with identifier '{1}' for order item id '{2}'.", productType, identifier, orderItemId));
        }
    }
}