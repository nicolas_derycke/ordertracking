﻿using Billing.API.Common.Interface.Contracts.Data;
using Billing.API.Common.Interface.Contracts.Requests;
using Billing.API.Common.Interface.Contracts.Services;
using Billing.API.Common.Interface.Contracts.Types;
using Core.Security;
using Intelligent.Core.Models;
using Intelligent.Shared.Core;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.ServiceModel.Client;
using OrderTracking.Messages.OrderVerification;
using OrderVerification.Service.Data;
using OrderVerification.Service.Messages;
using OrderVerification.Service.Proxy;
using OrderVerification.Service.Queries;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderVerification.Service.Rules.OrderTracking
{
    public class MailQueueShouldContainMailRule : IVerificationRule<DelayedOrderFinishedVerification>
    {
        private readonly ITokenProvider _tokenProvider;
        private readonly IQueryHandler _queryHandler;
        private readonly IServiceInvoker<IInvoiceService> _invoiceServiceInvoker;

        public MailQueueShouldContainMailRule(ITokenProvider tokenProvider, IQueryHandler queryHandler, IServiceInvoker<IInvoiceService> invoiceServiceInvoker)
        {
            if (tokenProvider == null) throw new ArgumentNullException("tokenProvider");
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (invoiceServiceInvoker == null) throw new ArgumentNullException("invoiceServiceInvoker");

            this._tokenProvider = tokenProvider;
            this._queryHandler = queryHandler;
            this._invoiceServiceInvoker = invoiceServiceInvoker;
        }

        public async Task<IEnumerable<VerificationResult>> Verify(DelayedOrderFinishedVerification message)
        {
            var orderCode = message.OrderCode;

            if (!OrderVerificationHelper.CheckIfOrderCodeTypeIsTypeOfStore(orderCode))
                return new List<VerificationResult>() { VerificationResult.Succes(orderCode) };

            var storeApiProxy = new StoreApiProxy(_tokenProvider);
            var order = await storeApiProxy.GetOrderContract(orderCode);
            var invoiceDetail = _invoiceServiceInvoker.Invoke(c => c.GetInvoice(new GetInvoiceDetailRequest() { OrderCode = orderCode }));
            
            var orderMails = await _queryHandler.ExecuteAsync(new GetMailQuery(orderCode));

            var verificationContext = new MailVerificationContext(
                orderMails,
                order,
                invoiceDetail);

            var verificationResults = new List<VerificationResult>();
            verificationResults.AddRange(VerifyMailQueueItem(verificationContext, "Order confirmation"));
            
            if (await MailQueueShouldContainOrderProformaMail(verificationContext))
            {
                verificationResults.AddRange(VerifyMailQueueItem(verificationContext, "Order proforma"));
                verificationResults.Add(await VerifyInvoiceMailQueueItem(verificationContext, "Order proforma"));
            }
            else
            {
                verificationResults.AddRange(VerifyMailQueueItemNotExist(verificationContext, "Order proforma"));
            }

            if (verificationContext.Order.CheckoutType == CheckoutTypeContract.DirectActivation || verificationContext.Order.CheckoutType == CheckoutTypeContract.Free)
                verificationResults.AddRange(VerifyMailQueueItemNotExist(verificationContext, "Order payment confirmation"));
            else
                verificationResults.AddRange(VerifyMailQueueItem(verificationContext, "Order payment confirmation"));
            
            if(verificationContext.Order.Items.Any(i => i.ProductCode == "pre-registration" && !i.IsEap()))
                verificationResults.AddRange(VerifyMailQueueItem(verificationContext, "Pre-registration activation"));
            else
                verificationResults.AddRange(VerifyMailQueueItemNotExist(verificationContext, "Pre-registration activation"));

            if (verificationContext.Order.Items.Any(i => i.ProductCode == "pre-registration" && i.IsEap()))
                verificationResults.AddRange(VerifyMailQueueItem(verificationContext, "Pre-registration activation eap"));
            else
                verificationResults.AddRange(VerifyMailQueueItemNotExist(verificationContext, "Pre-registration activation eap"));

            if (verificationResults.Where(r => r != null).Count() == 0)
                verificationResults.Add(VerificationResult.Succes(orderCode));

            return verificationResults;
        }

        private Optional<VerificationResult> VerifyMailQueueItemNotExist(MailVerificationContext verificationContext, string description)
        {
            if (verificationContext.MailQueueItems.Any(i => i.Description == description))
                return VerificationResult.Fail(
                        verificationContext.Order.OrderCode,
                        VerificationRule.MailQueueShouldContainMail,
                        string.Format("Mail queue shouldn't contain the {0} mail", description));

            return Optional<VerificationResult>.Empty;
        }

        private Optional<VerificationResult> VerifyMailQueueItem(MailVerificationContext verificationContext, string description)
        {
            var orderCode = verificationContext.Order.OrderCode;
            var customerIdentifier = verificationContext.CustomerIdentifier;
            MailQueueItem mailQueueItem = GetMailQueueItemByDescription(verificationContext, description);

            if (mailQueueItem == null)
                return VerificationResult.Fail(
                    orderCode,
                    VerificationRule.MailQueueShouldContainMail,
                    string.Format("No mail found with description '{0}'", description));

            if (!mailQueueItem.MailSubject.Contains(orderCode))
                return VerificationResult.Fail(
                    orderCode,
                    VerificationRule.MailQueueShouldContainMail,
                    string.Format("{0} mail subject does not contain ordercode: '{1}'", description, mailQueueItem.MailSubject));

            if (mailQueueItem.OrderCode != orderCode)
                return VerificationResult.Fail(
                    orderCode,
                    VerificationRule.MailQueueShouldContainMail,
                    string.Format("{0} mail contains a wrong ordercode property: '{1}'", description, mailQueueItem.OrderCode));
            
            if (mailQueueItem.CustomerID != customerIdentifier.CustomerId)
                return VerificationResult.Fail(
                    orderCode,
                    VerificationRule.MailQueueShouldContainMail,
                    string.Format("{0} mail contains a wrong customer id property: '{1}'", description, mailQueueItem.CustomerID));

            return Optional<VerificationResult>.Empty;
        }

        private MailQueueItem GetMailQueueItemByDescription(MailVerificationContext verificationContext, string description)
        {
            return verificationContext.MailQueueItems.FirstOrDefault(m => m.Description == description);
        }

        private async Task<VerificationResult> VerifyInvoiceMailQueueItem(MailVerificationContext verificationContext, string description)
        {
            var mailQueueItem = verificationContext.MailQueueItems.FirstOrDefault(i => i.Description == description);
            var mailQueueContainOrderProformaMail = await MailQueueShouldContainOrderProformaMail(verificationContext);
            if (mailQueueContainOrderProformaMail && mailQueueItem != null && mailQueueItem.InvoiceID != verificationContext.InvoiceDetail.InvoiceID)
                return VerificationResult.Fail(
                    verificationContext.Order.OrderCode,
                    VerificationRule.MailQueueShouldContainMail,
                    string.Format("{0} mail contains a wrong invoice id property: '{1}'", description, mailQueueItem.InvoiceID));

            return null;
        }

        private async Task<bool> MailQueueShouldContainOrderProformaMail(MailVerificationContext verificationContext)
        {
            if (verificationContext.InvoiceDetail == null)
                return false;

            var invoiceType = verificationContext.InvoiceDetail.InvoiceType;
            var checkoutType = verificationContext.Order.CheckoutType;
            
            if (invoiceType == InvoiceTypeDataContract.NewOrdersProforma || invoiceType == InvoiceTypeDataContract.PeriodFreeProforma)
                return false;

            if (checkoutType == CheckoutTypeContract.CreditCardPayment || checkoutType == CheckoutTypeContract.DebitCardPayment || checkoutType == CheckoutTypeContract.DirectActivation || checkoutType == CheckoutTypeContract.Free)
                return false;

            var hasCustomerEmailReceiveSetting = await _queryHandler.ExecuteAsync(new HasCustomerEmailReceiveSettingQuery(verificationContext.CustomerIdentifier.CustomerId));
            if(!hasCustomerEmailReceiveSetting)
                return false;

            return true;
        }
    }

    internal class MailVerificationContext
    {
        public MailVerificationContext(IEnumerable<MailQueueItem> mailQueueItems, OrderContract order, InvoiceDetailDataContract invoiceDetail)
        {
            MailQueueItems = mailQueueItems;
            InvoiceDetail = invoiceDetail;
            Order = order;
        }

        public IEnumerable<MailQueueItem> MailQueueItems { get; private set; }
        public OrderContract Order { get; private set; }
        public CustomerIdentifier CustomerIdentifier { get { return CustomerIdentifier.FromCustomerNumber(Order.CustomerNumber); } }
        public InvoiceDetailDataContract InvoiceDetail { get; private set; }
    }
}