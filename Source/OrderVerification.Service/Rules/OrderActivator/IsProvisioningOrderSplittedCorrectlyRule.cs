﻿using Activator.Messages.Events;
using Combell.Model.OrderQueue;
using Core.Security;
using Intelligent.Shared.Core.Queries;
using OrderTracking.Messages.OrderVerification;
using OrderVerification.Service.Proxy;
using OrderVerification.Service.Queries;
using Store.API.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderVerification.Service.Rules.OrderActivator
{
    public class IsProvisioningOrderSplittedCorrectlyRule : IVerificationRule<OrderActivated>
    {
        private readonly IQueryHandler _queryHandler;
        private readonly ITokenProvider _tokenProvider;

        private static Dictionary<string, IEnumerable<OrderActionTypes>> _productTypeProvisiongActionTypeAndMapping = new Dictionary<string, IEnumerable<OrderActionTypes>>()
        {
            {"domainname", new [] { OrderActionTypes.ACT_InitializeUAC }},
            {"basic-e-mail", new [] { OrderActionTypes.MPV_InitializeUAC }},
            {"business-e-mail", new [] { OrderActionTypes.HE_InitializeUAC, OrderActionTypes.HE_Activation }},
            {"windows-shared-hosting", new [] { OrderActionTypes.HOS_InitializeUAC, OrderActionTypes.HOS_CreateHosting }},
            {"drupal", new [] { OrderActionTypes.HOS_InitializeUAC, OrderActionTypes.HOS_CreateHosting }},
            {"joomla", new [] { OrderActionTypes.HOS_InitializeUAC, OrderActionTypes.HOS_CreateHosting }},
            {"magento", new [] { OrderActionTypes.HOS_InitializeUAC, OrderActionTypes.HOS_CreateHosting }},
            {"wordpress", new [] { OrderActionTypes.HOS_InitializeUAC, OrderActionTypes.HOS_CreateHosting }},
            {"sitebuilder", new [] { OrderActionTypes.WPL_InitializeUAC, OrderActionTypes.WPL_AddAccount }},
            {"mysql-db", new [] {OrderActionTypes.HOS_DatabaseActivation }},
            {"ms-sql-db", new [] { OrderActionTypes.HOS_DatabaseActivation }},
            {"faxdiensten", new [] { OrderActionTypes.FAX_InitializeUAC }},

            {"standard-domain-validation", new [] { OrderActionTypes.SSL_InitializeUAC, OrderActionTypes.SSL_CreateIdentifier }},
            {"multi-domain-domain-validation", new [] { OrderActionTypes.SSL_InitializeUAC, OrderActionTypes.SSL_CreateIdentifier }},
            {"wildcard-domain-validation", new [] { OrderActionTypes.SSL_InitializeUAC, OrderActionTypes.SSL_CreateIdentifier }},
            {"standard-organisation", new [] { OrderActionTypes.SSL_InitializeUAC, OrderActionTypes.SSL_CreateIdentifier }},
            {"multi-domain-organisation", new [] { OrderActionTypes.SSL_InitializeUAC, OrderActionTypes.SSL_CreateIdentifier }},
            {"wildcard-organisation", new [] { OrderActionTypes.SSL_InitializeUAC, OrderActionTypes.SSL_CreateIdentifier }},
            {"standard-ev", new [] { OrderActionTypes.SSL_InitializeUAC, OrderActionTypes.SSL_CreateIdentifier }},
            {"multi-domain-ev", new [] { OrderActionTypes.SSL_InitializeUAC, OrderActionTypes.SSL_CreateIdentifier }},
            {"uniek-ip-adres", new [] { OrderActionTypes.HOS_UniqueIPActivation }}
        };

        private static Dictionary<string, IEnumerable<OrderActionTypes>> _productTypeProvisiongActionTypeOrMapping = new Dictionary<string, IEnumerable<OrderActionTypes>>()
        {
            {"domainname", new [] { OrderActionTypes.ACT_CreateDomain, OrderActionTypes.ACT_TradeDomain, OrderActionTypes.ACT_TransferDomain, OrderActionTypes.ACT_InternalTransferDomain, OrderActionTypes.ACT_MoveDomain }},
            {"faxdiensten", new [] { OrderActionTypes.FAX_FaxNgcPortNumber, OrderActionTypes.FAX_FaxNgcRequestNumber, OrderActionTypes.FAX_CreateFaxAccount }},
            {"caching", new [] { OrderActionTypes.UAC_AddAccountAddon, OrderActionTypes.UAC_UpgradeAccount }},
            {"linux-shared-hosting", new [] { OrderActionTypes.HOS_InitializeUAC, OrderActionTypes.HOS_CreateHosting, OrderActionTypes.UAC_UpgradeAccount }}
        };

        public IsProvisioningOrderSplittedCorrectlyRule(IQueryHandler queryHandler, ITokenProvider tokenProvider)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (tokenProvider == null) throw new ArgumentNullException("tokenProvider");

            this._queryHandler = queryHandler;
            this._tokenProvider = tokenProvider;
        }

        public async Task<IEnumerable<VerificationResult>> Verify(OrderActivated message)
        {
            if (!OrderVerificationHelper.CheckIfOrderCodeTypeIsTypeOfStore(message.OrderCode))
                return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };

            var provisioningOrderId = await _queryHandler.ExecuteAsync(new GetProvisioningOrderIdQuery(message.OrderCode));
            var storeApiProxy = new StoreApiProxy(_tokenProvider);
            var order = await storeApiProxy.GetOrderContract(message.OrderCode);

            var verificationResult = OrderVerificationHelper.CheckProvisioningOrder(order, provisioningOrderId, message.OrderCode, VerificationRule.OrderShouldBeProvisionedActions);
            if (verificationResult.HasValue)
                return new List<VerificationResult>() { verificationResult.Value };

            var verificationResults = new List<VerificationResult>();
            var provisioningOrderActions = await _queryHandler.ExecuteAsync(new GetProvisioningOrderActionsQuery(provisioningOrderId.Value));
            foreach (var orderItem in order.Items.Where(i => !i.ParentId.HasValue))
            {
                if (orderItem.ProductCode == "pre-registration")
                    break;

                if (orderItem.Attributes.Any(a => a.Key == AttributeKey.ManualActivation) || orderItem.Attributes.Count == 0)
                {
                    if (orderItem.ProductCode == "reseller-platform")
                    {
                        if (!provisioningOrderActions.Any(oat => (OrderActionTypes)oat.OrderActionTypeID == OrderActionTypes.UAC_CheckResellerProfile))
                        {
                            verificationResults.Add(VerificationResult.Fail(
                                message.OrderCode,
                                VerificationRule.OrderShouldBeProvisionedActions,
                                string.Format(
                                    "Order item id '{0}' with product code 'reseller-platform' is not splitted correctly",
                                    orderItem.OrderItemId)));
                        }
                    }
                    else if (!provisioningOrderActions.Any(oat => (OrderActionTypes)oat.OrderActionTypeID == OrderActionTypes.MAN_ActivateProduct))
                    {
                        verificationResults.Add(VerificationResult.Fail(
                            message.OrderCode,
                            VerificationRule.OrderShouldBeProvisionedActions,
                            string.Format(
                                "Order item id '{0}' with product code '{1}' is not splitted correctly",
                                orderItem.OrderItemId,
                                orderItem.ProductCode)));
                    }

                    break;
                }

                if (!(_productTypeProvisiongActionTypeOrMapping.ContainsKey(orderItem.ProductType) || _productTypeProvisiongActionTypeAndMapping.ContainsKey(orderItem.ProductType)))
                    throw new Exception(string.Format("Product type '{0}' is not mapped to an action type", orderItem.ProductType));

                if (_productTypeProvisiongActionTypeOrMapping.ContainsKey(orderItem.ProductType))
                {
                    var requiredOrActionTypes = _productTypeProvisiongActionTypeOrMapping[orderItem.ProductType];
                    if (!requiredOrActionTypes.Any(roat => provisioningOrderActions.Any(oat => (OrderActionTypes)oat.OrderActionTypeID == roat)))
                        verificationResults.Add(VerificationResult.Fail(
                            message.OrderCode,
                            VerificationRule.OrderShouldBeProvisionedActions,
                            string.Format(
                                "action could not be found for product type '{0}' in order item id '{1}'",
                                orderItem.ProductType,
                                orderItem.OrderItemId)));
                }

                if (_productTypeProvisiongActionTypeAndMapping.ContainsKey(orderItem.ProductType))
                {
                    var requiredAndActionTypes = _productTypeProvisiongActionTypeAndMapping[orderItem.ProductType];
                    if (!requiredAndActionTypes.All(raat => provisioningOrderActions.Any(oat => (OrderActionTypes)oat.OrderActionTypeID == raat)))
                        verificationResults.Add(VerificationResult.Fail(
                            message.OrderCode,
                            VerificationRule.OrderShouldBeProvisionedActions,
                            string.Format(
                                "action could not be found for product type '{0}' in order item id '{1}'",
                                orderItem.ProductType,
                                orderItem.OrderItemId)));
                }
            }

            if (verificationResults.Where(r => r != null).Count() == 0)
                verificationResults.Add(VerificationResult.Succes(message.OrderCode));

            return verificationResults;
        }
    }
}