﻿using Activator.Messages.Events;
using Core.Security;
using Intelligent.Shared.Core.Queries;
using OrderTracking.Messages.OrderVerification;
using OrderVerification.Service.Proxy;
using OrderVerification.Service.Queries;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrderVerification.Service.Rules.OrderActivator
{
    public class IsOrderProvisionedRule : IVerificationRule<OrderActivated>
    {
        private readonly ITokenProvider _tokenProvider;
        private readonly IQueryHandler _queryHandler;

        public IsOrderProvisionedRule(ITokenProvider tokenProvider, IQueryHandler queryHandler)
        {
            if (tokenProvider == null) throw new ArgumentNullException("tokenProvider");
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            this._tokenProvider = tokenProvider;
            this._queryHandler = queryHandler;
        }

        public async Task<IEnumerable<VerificationResult>> Verify(OrderActivated message)
        {
            if (!OrderVerificationHelper.CheckIfOrderCodeTypeIsTypeOfStore(message.OrderCode))
                return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };

            var provisioningOrderId = await _queryHandler.ExecuteAsync(new GetProvisioningOrderIdQuery(message.OrderCode));
            var storeApiProxy = new StoreApiProxy(_tokenProvider);
            var order = await storeApiProxy.GetOrderContract(message.OrderCode);

            var verificationResult = OrderVerificationHelper.CheckProvisioningOrder(order, provisioningOrderId, message.OrderCode, VerificationRule.OrderShouldBeProvisionedActions);
            if (verificationResult.HasValue)
                return new List<VerificationResult>() { verificationResult.Value };

            return new List<VerificationResult>() { VerificationResult.Succes(message.OrderCode) };
        }
    }
}