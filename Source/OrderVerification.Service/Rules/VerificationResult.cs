﻿using OrderTracking.Messages.OrderVerification;

namespace OrderVerification.Service.Rules
{
    public class VerificationResult
    {
        public bool IsValid { get; private set; }
        public bool IsNotValid { get { return !IsValid; } }
        public string OrderCode { get; private set; }
        public VerificationRule? Rule { get; private set; }
        public string Result { get; private set; }

        private VerificationResult(string orderCode)
        {
            this.IsValid = true;
            this.OrderCode = orderCode;
        }

        private VerificationResult(string orderCode, VerificationRule rule, string result)
        {
            this.IsValid = false;
            this.OrderCode = orderCode;
            this.Rule = rule;
            this.Result = result;
        }

        public static VerificationResult Succes(string orderCode)
        {
            return new VerificationResult(orderCode);
        }

        public static VerificationResult Fail(string orderCode, VerificationRule rule, string result)
        {
            return new VerificationResult(orderCode, rule, result);
        }
    }
}