﻿using Core.Security;
using Intelligent.Shared.Net.Http;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.Orders;
using System.Configuration;
using System.Threading.Tasks;

namespace OrderVerification.Service.Proxy
{
    public class StoreApiProxy
    {
        private readonly ITokenProvider _tokenProvider;

        public StoreApiProxy(ITokenProvider tokenProvider)
        {
            _tokenProvider = tokenProvider;
        }

        public async Task<BasketContract> GetCheckedoutBasketContract(string orderCode)
        {
            var url = string.Format("{0}/checkedoutbaskets/{1}", ConfigurationManager.AppSettings["StoreApiUrl"].ToString(), orderCode);
            var client = CreateStronglyTypedHttpClient();
            return await client.GetAsync<BasketContract>(url);
        }

        public async Task<OrderContract> GetOrderContract(string orderCode)
        {
            var url = string.Format("{0}/orders/{1}", ConfigurationManager.AppSettings["StoreApiUrl"].ToString(), orderCode);
            var client = CreateStronglyTypedHttpClient();
            return await client.GetAsync<OrderContract>(url);
        }

        public async Task<OrderStateContract> GetOrderState(string orderCode)
        {
            var url = string.Format("{0}/orderstates/{1}", ConfigurationManager.AppSettings["StoreApiUrl"].ToString(), orderCode);
            var client = CreateStronglyTypedHttpClient();
            return await client.GetAsync<OrderStateContract>(url);
        }

        private StronglyTypedHttpClient CreateStronglyTypedHttpClient()
        {
            var accessToken = _tokenProvider.GetApplicationGrantToken("orderdomain.verifier").AccessToken;
            return StronglyTypedHttpClient.Create().WithBearerToken(accessToken).Build();
        }
    }
}