﻿using System;
using System.Threading.Tasks;
using MassTransit;
using OrderTracking.Messages;
using OrderVerification.Service.Messages;

namespace OrderVerification.Service.EventHandlers
{
    public class OrderFinishedHandler : IConsumer<OrderFinished>
    {        
        public async Task Consume(ConsumeContext<OrderFinished> context)
        {
            await context.ScheduleMessage(DateTime.Now.AddMinutes(5), new DelayedOrderFinishedVerification(context.Message.OrderCode));
        }
    }
}