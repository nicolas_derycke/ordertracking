﻿using Intelligent.Shared.Core.Commands;
using MassTransit;
using OrderTracking.Messages.OrderVerification;
using OrderVerification.Service.Commands;
using System;
using System.Threading.Tasks;

namespace OrderVerification.Service.EventHandlers
{
    public class OrderVerificationFailedHandler : IConsumer<OrderVerificationFailed>
    {
        private readonly ICommandHandler _commandHandler;

        public OrderVerificationFailedHandler(ICommandHandler commandHandler)
        {
            if (commandHandler == null) throw new ArgumentNullException("commandHandler");

            this._commandHandler = commandHandler;
        }

        public async Task Consume(ConsumeContext<OrderVerificationFailed> context)
        {
            var message = context.Message;
            await _commandHandler.ExecuteAsync(new CreateFailedVerification(message.OrderCode, message.Rule, message.Result, message.VerificationFailedOn));
        }
    }
}