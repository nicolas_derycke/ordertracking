﻿using MassTransit;
using OrderTracking.Messages.OrderVerification;
using OrderVerification.Service.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderVerification.Service.EventHandlers
{
    public class ExecuteRulesHandler<T> : IConsumer<T> where T : class
    {
        private readonly IEnumerable<IVerificationRule<T>> _rules;

        public ExecuteRulesHandler(IEnumerable<IVerificationRule<T>> rules)
        {
            if (rules == null) throw new ArgumentNullException("rules");

            this._rules = rules;
        }

        public async Task Consume(ConsumeContext<T> context)
        {
            foreach (var rule in _rules)
            {
                var results = await rule.Verify(context.Message);
                foreach (var result in results.Where(r => r != null && r.IsNotValid))
                    await context.Publish(new OrderVerificationFailed(result.OrderCode, result.Rule.Value, result.Result, DateTime.Now));
            }
        }
    }
}