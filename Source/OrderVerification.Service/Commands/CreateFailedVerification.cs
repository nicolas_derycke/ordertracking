﻿using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using OrderTracking.Messages.OrderVerification;
using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace OrderVerification.Service.Commands
{
    public class CreateFailedVerification : IAsyncCommand
    {
        public string OrderCode { get; private set; }
        public VerificationRule Rule { get; private set; }
        public string Result { get; private set; }
        public DateTime CreationDate { get; private set; }

        public CreateFailedVerification(string orderCode, VerificationRule rule, string result, DateTime creationDate)
        {
            this.OrderCode = orderCode;
            this.Rule = rule;
            this.Result = result;
            this.CreationDate = creationDate;
        }

        public async Task ExecuteAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                await connection.ExecuteAsync("[ordertracking].[CreateFailedVerification]",
                    new
                    {
                        OrderCode,
                        Rule = Rule.ToString(),
                        Result,
                        CreationDate
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}