﻿using Intelligent.Shared.Logging.Serilog;

namespace OrderVerification.Service
{
    class Program
    {
        static int Main(string[] args)
        {
            LogConfiguration.Initialize();

            var factory = new ServiceFactory();
            return factory.Run<Service>();
        }
    }
}