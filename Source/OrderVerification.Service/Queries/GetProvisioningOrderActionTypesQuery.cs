﻿using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using OrderVerification.Service.Data;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace OrderVerification.Service.Queries
{
    public class GetProvisioningOrderActionsQuery : IAsyncQuery<IEnumerable<OrderQueueActionData>>
    {
        public int OrderId { get; private set; }

        public GetProvisioningOrderActionsQuery(int orderId)
        {
            this.OrderId = orderId;
        }

        public async Task<IEnumerable<OrderQueueActionData>> ExecuteAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                return await connection.QueryAsync<OrderQueueActionData>(
                    "[dbo].[OM_GetOrderQueueItemDetails]",
                    new
                    {
                        OrderQueueID = OrderId
                    },
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}