﻿using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace OrderVerification.Service.Queries
{
    public class HasCustomerEmailReceiveSettingQuery : IAsyncQuery<bool>
    {
        public int CustomerId { get; private set; }

        private string _query =
            @"if(exists(SELECT *
	                    FROM [CombellGroup].[dbo].[CM_Customer_InvoiceReceiveMethod]
	                    WHERE CustomerID = @CustomerId
	                      AND InvoiceReceiveMethodID = 2))
              BEGIN
	            SELECT 1
              END
              ELSE
              BEGIN
	            SELECT 0
              END";

        public HasCustomerEmailReceiveSettingQuery(int customerId)
        {
            this.CustomerId = customerId;
        }

        public async Task<bool> ExecuteAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                return await connection.ExecuteScalarAsync<bool>(
                    _query,
                    new
                    {
                        CustomerId = CustomerId
                    });
            }
        }
    }
}