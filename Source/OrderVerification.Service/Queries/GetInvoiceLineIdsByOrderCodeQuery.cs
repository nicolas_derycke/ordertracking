﻿using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace OrderVerification.Service.Queries
{
    public class GetInvoiceLineIdsByOrderCodeQuery : IAsyncQuery<IEnumerable<int>>
    {
        public string OrderCode { get; private set; }

        private string _query =
            @"SELECT invLine.InvoicelineId
	          FROM [Billing].[InvoicedOrder] invOrd
	          INNER JOIN [Billing].[InvoicedOrder_Invoiceline] invLine
		          ON invOrd.Id = invLine.InvoicedOrderId
	          WHERE invOrd.OrderCode = @OrderCode";

        public GetInvoiceLineIdsByOrderCodeQuery(string orderCode)
        {
            this.OrderCode = orderCode;
        }

        public async Task<IEnumerable<int>> ExecuteAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                return await connection.QueryAsync<int>(
                    _query,
                    new
                    {
                        OrderCode = OrderCode
                    });
            }
        }
    }
}