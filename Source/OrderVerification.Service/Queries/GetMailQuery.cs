﻿using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using OrderVerification.Service.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OrderVerification.Service.Queries
{
    public class GetMailQuery : IAsyncQuery<IEnumerable<MailQueueItem>>
    {
        public string OrderCode { get; private set; }

        public GetMailQuery(string orderCode)
        {
            this.OrderCode = orderCode;
        }

        public async Task<IEnumerable<MailQueueItem>> ExecuteAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                var mailQueueItems = await connection.QueryAsync<MailQueueItem>(
                    "[dbo].[MAI_SearchMailQueueItems]",
                    new
                    {
                        MailID = 0,
                        CreatieDatum = (DateTime?)null,
                        CreatieDatumVan = (DateTime?)null,
                        CreatieDatumTot = (DateTime?)null,
                        CreatieOperator = (int?)null,
                        UpdateDatum = (DateTime?)null,
                        UpdateDatumVan = (DateTime?)null,
                        UpdateDatumTot = (DateTime?)null,
                        UpdateOperator = (int?)null,
                        CustomerID = 0,
                        MailServer = "",
                        MailSubject = "",
                        MailTos = "",
                        OrderCode = OrderCode,
                        Description = "",
                        CancelStatus = 0,
                        SendStatus = 0,
                        VerzendDatum = (DateTime?)null,
                        VerzendDatumVan = (DateTime?)null,
                        VerzendDatumTot = (DateTime?)null,
                        VerzendOperator = (int?)null,
                        VerzondenDatum = (DateTime?)null,
                        VerzondenDatumVan = (DateTime?)null,
                        VerzondenDatumTot = (DateTime?)null,
                        VerzondenOperator = (int?)null,
                        InvoiceID = 0
                    },
                    commandType: CommandType.StoredProcedure);
                
                foreach(var mailQueueItem in mailQueueItems)
                {
                    var mailItem = (await connection.QueryAsync<MailQueueItem>(
                        "[dbo].[MAI_LoadMailItemByID]",
                        new
                        {
                            MailID = mailQueueItem.MailId
                        },
                        commandType: CommandType.StoredProcedure)).FirstOrDefault();

                    if (mailItem == null)
                        throw new Exception(string.Format("Couldn't load mail id '{0}'", mailQueueItem.MailId));

                    mailQueueItem.MailBody = mailItem.MailBody;
                    mailQueueItem.MailSubject = mailItem.MailSubject;
                    mailQueueItem.OrderCode = mailItem.OrderCode;
                    mailQueueItem.InvoiceID = mailItem.InvoiceID;
                    mailQueueItem.Description = mailItem.Description;
                    mailQueueItem.Send = mailItem.Send;
                }

                return mailQueueItems;
            }
        }
    }
}