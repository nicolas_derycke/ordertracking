﻿using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace OrderVerification.Service.Queries
{
    public class GetProvisioningOrderIdQuery : IAsyncQuery<int?>
    {
        public string OrderCode { get; private set; }

        public GetProvisioningOrderIdQuery(string orderCode)
        {
            this.OrderCode = orderCode;
        }

        public async Task<int?> ExecuteAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return await connection.ExecuteScalarAsync<int?>(
                    "[dbo].[ORQ_GetOrderInfo]",
                    new
                    {
                        OrderCode = OrderCode
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}