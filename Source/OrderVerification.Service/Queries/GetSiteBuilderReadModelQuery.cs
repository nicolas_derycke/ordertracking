﻿using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OrderVerification.Service.Queries
{
    public class GetSiteBuilderReadModelQuery : IAsyncQuery<int>
    {
        public string Identifier { get; private set; }
        public int CustomerId { get; set; }

        private string _query = 
            @"SELECT AccountId
              FROM [CombellGroup].[uac].[SiteBuilder]
              WHERE CustomerId = @CustomerId
                AND Identifier = @Identifier
                AND IsAccessible = 1";

        public GetSiteBuilderReadModelQuery(string identifier, int customerId)
        {
            this.Identifier = identifier;
            this.CustomerId = customerId;
        }

        public async Task<int> ExecuteAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                return (await connection.QueryAsync<int>(
                    _query,
                    new
                    {
                        CustomerId = CustomerId,
                        Identifier = Identifier,
                    })).FirstOrDefault();
            }
        }
    }
}