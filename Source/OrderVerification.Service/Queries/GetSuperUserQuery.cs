﻿using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace OrderVerification.Service.Queries
{
    public class GetSuperUserQuery : IAsyncQuery<int>
    {
        public int CustomerId { get; private set; }

        public GetSuperUserQuery(int customerId)
        {
            this.CustomerId = customerId;
        }

        public async Task<int> ExecuteAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                return await connection.ExecuteScalarAsync<int>(
                    "[dbo].[UAC_GetUserByCustomerID]",
                    new
                    {
                        CustomerID = CustomerId
                    },
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}