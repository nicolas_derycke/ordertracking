﻿namespace OrderVerification.Service.Data
{
    public class MailQueueItem
    {
        public int MailId { get; set; }
        public int CustomerID { get; set; }
        public string MailBody { get; set; }
        public string MailSubject { get; set; }
        public string OrderCode { get; set; }
        public int InvoiceID { get; set; }
        public string Description { get; set; }
        public bool Send { get; set; }
    }
}