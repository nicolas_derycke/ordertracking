﻿namespace OrderVerification.Service.Data
{
    public class OrderQueueActionData
    {
        public int OrderActionQueueID { get; set; }
        public int OrderActionTypeID { get; set; }
        public int OrderActionStatusID { get; set; }
    }
}