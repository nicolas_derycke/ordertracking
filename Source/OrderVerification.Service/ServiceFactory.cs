﻿using Billing.API.Common.Interface.Contracts.Services;
using Core.Security;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Messaging.MassTransit3;
using Intelligent.Shared.Messaging.MassTransit3.Serilog;
using Intelligent.Shared.Reflection;
using Intelligent.Shared.ServiceModel.Client;
using Intelligent.Shared.Services.Topshelf;
using MassTransit;
using Microsoft.Practices.Unity;
using OrderVerification.Service.EventHandlers;
using OrderVerification.Service.Rules;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Ticketing.API.Interface.Contracts.Services;

namespace OrderVerification.Service
{
    internal class ServiceFactory : UnityTopshelfServiceFactory
    {
        public override void ConfigureContainer(IUnityContainer container)
        {
            container.RegisterInstance(Log.Logger);

            container.RegisterType<IQueryHandler, QueryHandler>(new ContainerControlledLifetimeManager());
            container.RegisterType<ICommandHandler, CommandHandler>(new ContainerControlledLifetimeManager());
            container.RegisterInstance<ITokenProvider>(new TokenProvider(Scopes.Customer, "intelligent.orderdomain.verifier", "verifier"));
            container.RegisterInstance<IServiceInvoker<IInvoiceService>>(new ServiceInvoker<IInvoiceService>("Billing_UAC_InvoiceServiceConfiguration"));
            container.RegisterInstance<IServiceInvoker<IPDFServerService>>(new ServiceInvoker<IPDFServerService>("PDFService_Generator_Configuration"));
            container.RegisterInstance<IServiceInvoker<ITicketingService>>(new ServiceInvoker<ITicketingService>("Ticketing_TicketingServiceConfiguration"));
            
            var verificationConsumers = RegisterVerificationRules(container);
            RegisterBus(container, verificationConsumers);
        }

        private IEnumerable<Type> RegisterVerificationRules(IUnityContainer container)
        {
            var verificationConsumsers = new List<Type>();
            var groupedRules = TypeFinder.FindTypesWhichImplement(typeof(IVerificationRule<>).Assembly, typeof(IVerificationRule<>)).GroupBy(r => r.GetInterfaces().First());
            foreach (var ruleGroup in groupedRules)
            {
                // register the consumer for the event used by the rule
                var eventType = ruleGroup.Key.GetGenericArguments().First();
                var verificationConsumer = typeof(IConsumer<>).MakeGenericType(eventType);
                container.RegisterType(verificationConsumer, typeof(ExecuteRulesHandler<>).MakeGenericType(eventType));
                verificationConsumsers.Add(verificationConsumer);

                // register all rules
                foreach (var rule in ruleGroup)
                    container.RegisterType(ruleGroup.Key, rule, rule.Name, new ContainerControlledLifetimeManager());

                // register the rules into an IEnumerable
                container.RegisterType(typeof(IEnumerable<>).MakeGenericType(ruleGroup.Key), ruleGroup.Key.MakeArrayType(), new ContainerControlledLifetimeManager());
            }
            return verificationConsumsers;
        }

        private static void RegisterBus(IUnityContainer container, IEnumerable<Type> verificationConsumers)
        {
            var allTypesToRegister =
                TypeFinder.FindTypesWhichImplement(
                    typeof(ServiceFactory).Assembly,
                    typeof(IConsumer<>)).
                Where(t => t != typeof(ExecuteRulesHandler<>));
            allTypesToRegister = allTypesToRegister.Concat(verificationConsumers);

            var busControl = BusControlBuilder.New(
                    ConfigurationManager.AppSettings["MassTransit_HostAddress"],
                    ConfigurationManager.AppSettings["MassTransit_UserName"],
                    ConfigurationManager.AppSettings["MassTransit_Password"]
                )
                .AddConsumerEndpoint("ordertracking.service.verification", ep =>
                {
                    ep.WithConcurrency(new ConcurrencySetting(1))
                        .ForTypes(
                            allTypesToRegister,
                            type => container.Resolve(type));
                })
                .UseSeriLog()
                .Build();

            container.RegisterBusControl(busControl);
        }
    }
}