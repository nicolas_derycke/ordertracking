using Billing.Messages;
using NUnit.Framework;
using Store.Messages.Events;
using Store.Messages.Payment;
using System;
using System.Threading.Tasks;
using Activator.Messages;
using Activator.Messages.Events;
using OrderManagement.Messages;
using OrderManagement.Messages.Events;

namespace OrderTracking.Service.Tests.StateTracking
{
    [TestFixture]
    public class When_using_state_machine_for_unpaid_reseller_order : OrderTrackingStateMachineInMemoryTestFixture
    {
        [Test]
        public async Task State_transitions_are_correct()
        {
            var checkoutCompletedOn = DateTime.Now;
            await When(new CheckoutCompleted(OrderCode, 4555, CheckoutType.DirectActivation, new CheckoutData(444, "ref"), "customer ref", checkoutCompletedOn, 1, "Jos", true));
            await ThenSagaStateIs(StateMachine.CheckOutCompleted);
            ThenOrderStateIsInitializedCorrectly(checkoutCompletedOn, 1, "Jos", true);

            await When(new OrderCreated(OrderCode, CheckoutType.DirectActivation, 4555, "ref", DateTime.Now));
            await ThenSagaStateIs(StateMachine.Created);

            await When(new OrderActivated(OrderCode, ActivationMethod.Direct, "reason", 111, "activationUser", new[] { OrderCode }, DateTime.Now));
            await ThenSagaStateIs(StateMachine.Processing);
            await ThenOrderProcessingEventIsPublished;

            await When(new OrderInvoiced(OrderCode, 123, "", InvoiceType.NewOrdersProforma, DateTime.Now, 777, 88));
            await ThenSagaStateIs(StateMachine.Processing);

            await When(new ProvisioningOrderStatusChanged(OrderCode, ProvisioningOrderStatusses.Processing, "", 6, DateTime.Now));
            await ThenSagaStateIs(StateMachine.Processing);

            await When(new ProvisioningOrderStatusChanged(OrderCode, ProvisioningOrderStatusses.Pending, "", 6, DateTime.Now));
            await ThenSagaStateIs(StateMachine.Processing);

            await When(new ProvisioningOrderStatusChanged(OrderCode, ProvisioningOrderStatusses.Finished, "", 6, DateTime.Now));
            await ThenSagaStateIs(StateMachine.Finished);
            await ThenOrderFinishedEventIsPublished;
        }
    }
}