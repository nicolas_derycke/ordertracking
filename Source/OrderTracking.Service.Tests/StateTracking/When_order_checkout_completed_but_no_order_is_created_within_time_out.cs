using System;
using System.Threading.Tasks;
using NUnit.Framework;
using OrderTracking.Messages.OrderVerification;
using Store.Messages.Events;
using Store.Messages.Payment;

namespace OrderTracking.Service.Tests.StateTracking
{
    [TestFixture]
    public class When_order_checkout_completed_but_no_order_is_created_within_time_out : OrderTrackingStateMachineInMemoryTestFixture
    {
        [Test]
        public async Task Verification_failed_is_published()
        {
            var verificationFailed = SubscribeHandler<OrderVerificationFailed>(x => x.Message.OrderCode == OrderCode && x.Message.Rule == VerificationRule.OrderShouldBeCreated);
            var checkoutCompletedOn = DateTime.Now;
            await When(new CheckoutCompleted(OrderCode, 4555, CheckoutType.BankTransfer, new CheckoutData(444, "ref"), "customer ref", checkoutCompletedOn, 1, "Jos", true));
            await ThenSagaStateIs(StateMachine.CheckOutCompleted);
            ThenOrderStateIsInitializedCorrectly(checkoutCompletedOn, 1, "Jos", true);

            await Task.Delay(DefaultTimeSpan);

            await verificationFailed.IsCompletedAfter(DefaultTimeout);
        }
    }
}