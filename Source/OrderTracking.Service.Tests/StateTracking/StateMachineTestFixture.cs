using System;
using MassTransit;
using MassTransit.QuartzIntegration;
using MassTransit.TestFramework;
using NUnit.Framework;
using Quartz;
using Quartz.Impl;

namespace OrderTracking.Service.Tests.StateTracking
{
    public class StateMachineTestFixture :
        InMemoryTestFixture
    {
        Uri _quartzQueueAddress;
        ISendEndpoint _quartzQueueSendEndpoint;

        public StateMachineTestFixture()
        {
            _quartzQueueAddress = new Uri("loopback://localhost/quartz");
        }

        /// <summary>
        /// The sending endpoint for the InputQueue
        /// </summary>
        protected ISendEndpoint QuartzQueueSendEndpoint
        {
            get { return _quartzQueueSendEndpoint; }
        }

        protected Uri QuartzQueueAddress
        {
            get { return _quartzQueueAddress; }
            set
            {
                if (Bus != null)
                    throw new InvalidOperationException("The LocalBus has already been created, too late to change the URI");

                _quartzQueueAddress = value;
            }
        }

        IScheduler _scheduler;

        protected override void ConfigureBus(IInMemoryBusFactoryConfigurator configurator)
        {
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            _scheduler = schedulerFactory.GetScheduler();

            configurator.UseInMemoryScheduler();
        }

        [TestFixtureSetUp]
        public void Setup_quartz_service()
        {
            _scheduler.JobFactory = new MassTransitJobFactory(Bus);
            _scheduler.Start();

            _quartzQueueSendEndpoint = GetSendEndpoint(_quartzQueueAddress).Result;
        }

        [TestFixtureTearDown]
        public void Teardown_quartz_service()
        {
            if (_scheduler == null) return;
            _scheduler.Standby();
            _scheduler.Shutdown();
        }
    }
}