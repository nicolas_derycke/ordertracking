﻿using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace OrderTracking.Service.Tests.StateTracking
{
    public static class TaskExtensions
    {
        public static async Task IsCompletedAfter(this Task task, TimeSpan timeSpan)
        {
            Assert.That(task == await Task.WhenAny(task, Task.Delay(timeSpan)));
        }

        public static async Task IsNotCompletedAfter(this Task task, TimeSpan timeSpan)
        {
            Assert.That(task == await Task.WhenAny(task, Task.Delay(timeSpan)), Is.False);
        }
    }
}