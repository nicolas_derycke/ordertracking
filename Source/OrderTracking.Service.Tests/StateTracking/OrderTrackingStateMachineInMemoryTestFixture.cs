﻿using Automatonymous;
using Billing.Messages;
using FakeItEasy;
using Intelligent.Shared.Core.Commands;
using MassTransit;
using MassTransit.Saga;
using MassTransit.TestFramework;
using NUnit.Framework;
using OrderTracking.Messages;
using OrderTracking.Service.StateTracking;
using OrderTracking.Service.StateTracking.Commands;
using System;
using System.Threading.Tasks;

namespace OrderTracking.Service.Tests.StateTracking
{
    public abstract class OrderTrackingStateMachineInMemoryTestFixture : StateMachineTestFixture
    {
        protected const int DefaultTimeOutInSeconds = 3;
        protected readonly string OrderCode = "COM-4444-444";

        protected static readonly TimeSpan DefaultTimeSpan = TimeSpan.FromSeconds(40);
        protected static readonly TimeSpan DefaultTimeout = TimeSpan.FromSeconds(DefaultTimeOutInSeconds);

        protected OrderTrackingStateMachine StateMachine;
        protected InMemorySagaRepository<OrderSaga> Repository;
        protected ICommandHandler CommandHandler;
        protected IOrderTrackingConfiguration Configuration;
        protected Task ThenOrderProcessingEventIsPublished;
        protected Task ThenOrderFailedEventIsPublished;
        protected Task ThenOrderFinishedEventIsPublished;
        protected Task ThenOrderInvoicedEventIsPublished;

        protected override void ConfigureInputQueueEndpoint(IInMemoryReceiveEndpointConfigurator configurator)
        {
            CommandHandler = A.Fake<ICommandHandler>();
            Configuration = A.Fake<IOrderTrackingConfiguration>();
            A.CallTo(() => Configuration.MaxTimeBetweenCheckOutAndCreation).Returns(DefaultTimeSpan);

            StateMachine = new OrderTrackingStateMachine(CommandHandler, Configuration);
            Repository = new InMemorySagaRepository<OrderSaga>();

            configurator.StateMachineSaga(StateMachine, Repository);
        }

        protected override void ConfigureBus(IInMemoryBusFactoryConfigurator configurator)
        {
            base.ConfigureBus(configurator);

            configurator.UseMessageScheduler(QuartzQueueAddress);
        }

        [TestFixtureSetUp]
        public void Setup()
        {
            ThenOrderProcessingEventIsPublished = SubscribeHandler<OrderProcessingStarted>(x => x.Message.OrderCode == OrderCode).IsCompletedAfter(DefaultTimeout);
            ThenOrderFailedEventIsPublished = SubscribeHandler<OrderFailed>(x => x.Message.OrderCode == OrderCode).IsCompletedAfter(DefaultTimeout);
            ThenOrderFinishedEventIsPublished = SubscribeHandler<OrderFinished>(x => x.Message.OrderCode == OrderCode).IsCompletedAfter(DefaultTimeout);
            ThenOrderInvoicedEventIsPublished = SubscribeHandler<OrderInvoiced>(x => x.Message.OrderCode == OrderCode).IsCompletedAfter(DefaultTimeout);
        }

        protected async Task When<TEvent>(TEvent @event) where TEvent : class
        {
            await InputQueueSendEndpoint.Send(@event);
        }

        protected async Task ThenSagaStateIs(State expectedSagaState)
        {
            var guid = await Repository.ShouldContainSaga(x => x.OrderCode == OrderCode && x.OrderStatus == expectedSagaState.Name, DefaultTimeSpan);
            Assert.That(guid.HasValue, string.Format("No saga instance found with state '{0}'", expectedSagaState.Name));
        }

        protected void ThenOrderStateIsInitializedCorrectly(DateTime expectedCreationDate, int initiatedById, string initiatedBy, bool isInitiatedByEmployee)
        {
            A.CallTo(() => CommandHandler.Execute(A<CreateOrderStateCommand>.That.Matches(c =>
                c.OrderCode == OrderCode &&
                c.ActivationStatus == OrderActivationStatus.NotActivated &&
                c.CreationDate == expectedCreationDate &&
                c.InvoicingStatus == OrderInvoicingStatus.NotInvoiced &&
                c.PaymentStatus == OrderPaymentStatus.NotPaid &&
                c.ProvisioningStatus == OrderProvisioningStatus.NotProvisioned &&
                c.InitiatedBy == initiatedBy &&
                c.InitiatedById == initiatedById &&
                c.IsInitiatedByEmployee == isInitiatedByEmployee)))
                .MustHaveHappenedAfter(TimeSpan.FromSeconds(DefaultTimeOutInSeconds));
        }
    }
}