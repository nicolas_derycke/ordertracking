using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Logging;
using MassTransit.QuartzIntegration;
using MassTransit.Util;
using Quartz;

namespace OrderTracking.Service.Tests.StateTracking
{
    public class InMemorySchedulerBusObserver :
        IBusObserver
    {
        readonly ILog _log = Logger.Get<InMemorySchedulerBusObserver>();
        readonly IScheduler _scheduler;

        public InMemorySchedulerBusObserver(IScheduler scheduler)
        {
            _scheduler = scheduler;
        }

        public Task PostCreate(IBus bus)
        {
            return TaskUtil.Completed;
        }

        public Task CreateFaulted(Exception exception)
        {
            return TaskUtil.Completed;
        }

        public Task PreStart(IBus bus)
        {
            return TaskUtil.Completed;
        }

        public async Task PostStart(IBus bus, Task busReady)
        {
            if (_log.IsDebugEnabled)
                _log.DebugFormat("Quartz Scheduler Starting: {0} ({1}/{2})", bus.Address, _scheduler.SchedulerName, _scheduler.SchedulerInstanceId);

            await busReady.ConfigureAwait(false);

            _scheduler.JobFactory = new MassTransitJobFactory(bus);
            _scheduler.Start();

            if (_log.IsInfoEnabled)
                _log.InfoFormat("Quartz Scheduler Started: {0} ({1}/{2})", bus.Address, _scheduler.SchedulerName, _scheduler.SchedulerInstanceId);
        }

        public Task StartFaulted(IBus bus, Exception exception)
        {
            return TaskUtil.Completed;
        }

        public Task PreStop(IBus bus)
        {
            _scheduler.Standby();

            if (_log.IsInfoEnabled)
                _log.InfoFormat("Quartz Scheduler Paused: {0} ({1}/{2})", bus.Address, _scheduler.SchedulerName, _scheduler.SchedulerInstanceId);

            return TaskUtil.Completed;
        }

        public Task PostStop(IBus bus)
        {
            _scheduler.Shutdown();

            if (_log.IsInfoEnabled)
                _log.InfoFormat("Quartz Scheduler Stopped: {0} ({1}/{2})", bus.Address, _scheduler.SchedulerName, _scheduler.SchedulerInstanceId);

            return TaskUtil.Completed;
        }

        public Task StopFaulted(IBus bus, Exception exception)
        {
            return TaskUtil.Completed;
        }
    }
}