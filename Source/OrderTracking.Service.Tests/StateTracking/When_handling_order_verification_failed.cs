﻿using System;
using FakeItEasy;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Testing.NUnit;
using MassTransit;
using NUnit.Framework;
using OrderTracking.Messages.OrderVerification;
using OrderTracking.Service.StateTracking.Activities;
using OrderTracking.Service.StateTracking.Commands;

namespace OrderTracking.Service.Tests.StateTracking
{
    [TestFixture]
    public class When_handling_order_verification_failed : GivenWhenThen
    {
        private OrderVerificationFailedHandler _handler;
        private ICommandHandler _commandHandler;
        private ConsumeContext<OrderVerificationFailed> _consumeContext;
        private DateTime _verificationFailedOn;
        private string _orderCode;

        protected override void Given()
        {
            _commandHandler = A.Fake<ICommandHandler>();
            _handler = new OrderVerificationFailedHandler(_commandHandler);
            _consumeContext = A.Fake<ConsumeContext<OrderVerificationFailed>>();
            _orderCode = "COM-BLABLA";
            _verificationFailedOn = DateTime.Now;
            var orderVerificationFailed = new OrderVerificationFailed(_orderCode, VerificationRule.OrderShouldBeCreated, "Order not created after 5m", _verificationFailedOn);
            A.CallTo(() => _consumeContext.Message).Returns(orderVerificationFailed);
        }

        protected override void When()
        {
            _handler.Consume(_consumeContext).Wait();
        }

        [Test]
        public void Activity_is_logged_correclty()
        {
            A.CallTo(() => _commandHandler.Execute(A<CreateOrderActivityCommand>.That.Matches(c =>
                c.OrderCode == _orderCode &&
                c.Message == "Order verification failed for rule 'OrderCreated': Order not created after 5m" &&
                c.OccuredOn == _verificationFailedOn)));
        }
    }
}