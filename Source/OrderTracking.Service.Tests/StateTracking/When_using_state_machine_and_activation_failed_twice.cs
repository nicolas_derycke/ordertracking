using System;
using System.Threading.Tasks;
using Activator.Messages.Events;
using Billing.Messages;
using NUnit.Framework;
using Store.Messages.Events;
using Store.Messages.Payment;

namespace OrderTracking.Service.Tests.StateTracking
{
    [TestFixture]
    public class When_using_state_machine_and_activation_failed_twice : OrderTrackingStateMachineInMemoryTestFixture
    {
        [Test]
        public async Task State_transitions_are_correct()
        {
            var checkoutCompletedOn = DateTime.Now;
            await When(new CheckoutCompleted(OrderCode, 4555, CheckoutType.BankTransfer, new CheckoutData(444, "ref"), "customer ref", checkoutCompletedOn, 1, "Jos", false));

            await ThenSagaStateIs(StateMachine.CheckOutCompleted);
            ThenOrderStateIsInitializedCorrectly(checkoutCompletedOn, 1, "Jos", false);

            await When(new OrderCreated(OrderCode, CheckoutType.BankTransfer, 4555, "ref", DateTime.Now));
            await ThenSagaStateIs(StateMachine.Created);

            await When(new OrderInvoiced(OrderCode, 123,"", InvoiceType.Invoice, DateTime.Now, 777, 88));
            await ThenSagaStateIs(StateMachine.Processing);
            await ThenOrderProcessingEventIsPublished;

            await When(new OrderPaid(OrderCode, DateTime.Now));
            await ThenSagaStateIs(StateMachine.Processing);
            
            await When(new OrderActivationFailed(OrderCode, DateTime.Now));
            await ThenSagaStateIs(StateMachine.Failed);
            await ThenOrderFailedEventIsPublished;

            await When(new OrderActivationFailed(OrderCode, DateTime.Now));
            await ThenSagaStateIs(StateMachine.Failed);
            await ThenOrderFailedEventIsPublished;
        }
    }
}