using System;
using System.Diagnostics;
using System.Threading;
using FakeItEasy;
using FakeItEasy.Configuration;

namespace OrderTracking.Service.Tests.StateTracking
{
    public static class FakeItEasyExtensions
    {
        public static void MustHaveHappenedAfter(this IAssertConfiguration assertConfiguration, TimeSpan time)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            while (stopwatch.Elapsed < time)
            {
                try
                {
                    assertConfiguration.MustHaveHappened();
                    Thread.Sleep(10);
                    return;
                }
                catch (Exception)
                {
                }
            }

            assertConfiguration.MustHaveHappened();
        }

    }
}