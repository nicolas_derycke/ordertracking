using System;
using MassTransit;
using MassTransit.QuartzIntegration;
using Quartz;
using Quartz.Impl;

namespace OrderTracking.Service.Tests.StateTracking
{
    public static class QuartzIntegrationExtensions
    {
        public static void UseInMemoryScheduler(this IBusFactoryConfigurator configurator)
        {
            if (configurator == null)
                throw new ArgumentNullException("configurator");

            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            var scheduler = schedulerFactory.GetScheduler();

            var specification = new InMemorySchedulerBusFactorySpecification(scheduler);
            configurator.AddBusFactorySpecification(specification);

            configurator.ReceiveEndpoint("quartz", e =>
            {
                configurator.UseMessageScheduler(e.InputAddress);

                e.Consumer(() => new ScheduleMessageConsumer(scheduler));
                e.Consumer(() => new CancelScheduledMessageConsumer(scheduler));
            });
        }
    }
}