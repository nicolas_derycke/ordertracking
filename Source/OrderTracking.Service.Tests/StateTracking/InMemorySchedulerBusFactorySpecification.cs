using System.Collections.Generic;
using MassTransit.Builders;
using MassTransit.Configurators;
using Quartz;

namespace OrderTracking.Service.Tests.StateTracking
{
    public class InMemorySchedulerBusFactorySpecification :
        IBusFactorySpecification
    {
        readonly IScheduler _scheduler;

        public InMemorySchedulerBusFactorySpecification(IScheduler scheduler)
        {
            _scheduler = scheduler;
        }

        public IEnumerable<ValidationResult> Validate()
        {
            yield break;
        }

        public void Apply(IBusBuilder builder)
        {
            var observer = new InMemorySchedulerBusObserver(_scheduler);

            builder.ConnectBusObserver(observer);
        }
    }
}