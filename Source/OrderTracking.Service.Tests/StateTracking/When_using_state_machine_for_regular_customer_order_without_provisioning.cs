﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Activator.Messages;
using Activator.Messages.Events;
using Billing.Messages;
using NUnit.Framework;
using Store.Messages.Events;
using Store.Messages.Payment;

namespace OrderTracking.Service.Tests.StateTracking
{
    [TestFixture]
    public class When_using_state_machine_for_regular_customer_order_without_provisioning : OrderTrackingStateMachineInMemoryTestFixture
    {
        [Test]
        public async Task State_transitions_are_correct()
        {
            var checkoutCompletedOn = DateTime.Now;
            await When(new CheckoutCompleted(OrderCode, 4555, CheckoutType.BankTransfer, new CheckoutData(444, "ref"), "customer ref", checkoutCompletedOn, 1, "Jos", false));

            await ThenSagaStateIs(StateMachine.CheckOutCompleted);
            ThenOrderStateIsInitializedCorrectly(checkoutCompletedOn, 1, "Jos", false);

            await When(new OrderCreated(OrderCode, CheckoutType.BankTransfer, 4555, "ref", DateTime.Now));
            await ThenSagaStateIs(StateMachine.Created);

            await Bus.Publish(new OrderInvoiced(OrderCode, 123, "", InvoiceType.NewCustomerOrderProforma, DateTime.Now, 777, 88));
            await ThenSagaStateIs(StateMachine.Created);

            await When(new OrderPaid(OrderCode, DateTime.Now));
            await ThenSagaStateIs(StateMachine.Processing);
            await ThenOrderProcessingEventIsPublished;

            var activationReason = "reason";
            var activationById = 111;
            var activationBy = "testuser";
            var activatedOn = DateTime.Now;
            var activationMethod = ActivationMethod.Manual;
            await When(new OrderActivated(OrderCode, activationMethod, activationReason, activationById, activationBy, Enumerable.Empty<string>(), activatedOn));
            await ThenSagaStateIs(StateMachine.Processing);

            await ThenSagaStateIs(StateMachine.Finished);
            await ThenOrderFinishedEventIsPublished;
        }
    }
}