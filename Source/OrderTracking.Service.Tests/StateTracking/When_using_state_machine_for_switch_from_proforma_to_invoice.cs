using System;
using System.Threading.Tasks;
using Billing.Messages;
using Invoicing.Messages.Events;
using NUnit.Framework;
using Store.Messages.Events;
using Store.Messages.Payment;
using InvoiceType = Billing.Messages.InvoiceType;

namespace OrderTracking.Service.Tests.StateTracking
{
    [TestFixture]
    public class When_using_state_machine_for_switch_from_proforma_to_invoice : OrderTrackingStateMachineInMemoryTestFixture
    {
        [Test]
        public async Task State_transitions_are_correct()
        {
            var checkoutCompletedOn = DateTime.Now;
            await When(new CheckoutCompleted(OrderCode, 4555, CheckoutType.BankTransfer, new CheckoutData(444, "ref"), "customer ref", checkoutCompletedOn, 1, "Jos", false));

            await ThenSagaStateIs(StateMachine.CheckOutCompleted);
            ThenOrderStateIsInitializedCorrectly(checkoutCompletedOn, 1, "Jos", false);

            await When(new OrderCreated(OrderCode, CheckoutType.BankTransfer, 4555, "salesref", DateTime.Now));
            await ThenSagaStateIs(StateMachine.Created);

            var invoicedOn = DateTime.Now;
            await When(new OrderInvoiced(OrderCode, 123, "", InvoiceType.NewCustomerOrderProforma, invoicedOn, 111, 444));
            await ThenSagaStateIs(StateMachine.Created);

            await When(new OrderInvoiceTypeChangedEvent(OrderCode, 123, "16448478", Invoicing.Messages.Events.InvoiceType.NewCustomerOrderProforma, Invoicing.Messages.Events.InvoiceType.Invoice, DateTime.Now));
            await ThenSagaStateIs(StateMachine.Processing);

        }
    }
}