using System;
using System.Linq;
using System.Threading.Tasks;
using Activator.Messages;
using Activator.Messages.Events;
using Billing.Messages;
using NUnit.Framework;
using OrderManagement.Messages;
using OrderManagement.Messages.Events;
using Store.Messages.Events;
using Store.Messages.Payment;

namespace OrderTracking.Service.Tests.StateTracking
{
    [TestFixture]
    public class When_using_state_machine_and_activation_failed : OrderTrackingStateMachineInMemoryTestFixture
    {
        [Test]
        public async Task State_transitions_are_correct()
        {
            var checkoutCompletedOn = DateTime.Now;
            await When(new CheckoutCompleted(OrderCode, 4555, CheckoutType.BankTransfer, new CheckoutData(444, "ref"), "customer ref", checkoutCompletedOn, 1, "Jos", false));

            await ThenSagaStateIs(StateMachine.CheckOutCompleted);
            ThenOrderStateIsInitializedCorrectly(checkoutCompletedOn, 1, "Jos", false);

            await When(new OrderCreated(OrderCode, CheckoutType.BankTransfer, 4555, "salesref", DateTime.Now));
            await ThenSagaStateIs(StateMachine.Created);

            await When(new OrderInvoiced(OrderCode, 123, "", InvoiceType.Invoice, DateTime.Now, 111, 444));
            await ThenSagaStateIs(StateMachine.Processing);
            await ThenOrderProcessingEventIsPublished;

            await When(new OrderPaid(OrderCode, DateTime.Now));
            await ThenSagaStateIs(StateMachine.Processing);

            var activationFailedOn = DateTime.Now;
            await When(new OrderActivationFailed(OrderCode, activationFailedOn));
            await ThenSagaStateIs(StateMachine.Failed);
            await ThenOrderFailedEventIsPublished;

            var activatedOn = DateTime.Now;
            await When(new OrderActivated(OrderCode, ActivationMethod.Payment, "reason", 6, "System", new[] { OrderCode }, activatedOn));
            await ThenSagaStateIs(StateMachine.Processing);

            var provisioningStatusChangedOn = DateTime.Now;
            await When(new ProvisioningOrderStatusChanged(OrderCode, ProvisioningOrderStatusses.Processing, "", 6, provisioningStatusChangedOn));
            await ThenSagaStateIs(StateMachine.Processing);

            provisioningStatusChangedOn = DateTime.Now;
            await When(new ProvisioningOrderStatusChanged(OrderCode, ProvisioningOrderStatusses.Pending, "", 6, provisioningStatusChangedOn));
            await ThenSagaStateIs(StateMachine.Processing);
            await ThenOrderProcessingEventIsPublished;

            provisioningStatusChangedOn = DateTime.Now;
            await When(new ProvisioningOrderStatusChanged(OrderCode, ProvisioningOrderStatusses.Finished, "", 6, provisioningStatusChangedOn));

            await ThenSagaStateIs(StateMachine.Finished);
            await ThenOrderFinishedEventIsPublished;
        }
    }
}