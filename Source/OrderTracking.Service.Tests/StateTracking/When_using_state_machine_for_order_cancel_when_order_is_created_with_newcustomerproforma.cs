﻿using Billing.Messages;
using NUnit.Framework;
using OrderTracking.Messages;
using Store.Messages.Events;
using Store.Messages.Payment;
using System;
using System.Threading.Tasks;

namespace OrderTracking.Service.Tests.StateTracking
{
    public class When_using_state_machine_for_order_cancel_when_order_is_created_with_newcustomerproforma : OrderTrackingStateMachineInMemoryTestFixture
    {
        [Test]
        public async Task State_transitions_are_correct()
        {
            var checkoutCompletedOn = DateTime.Now;
            await When(new CheckoutCompleted(OrderCode, 4555, CheckoutType.BankTransfer, new PaymentData(444, "ref"), "customer ref", checkoutCompletedOn, 1, "Jos", false));

            await ThenSagaStateIs(StateMachine.CheckOutCompleted);
            ThenOrderStateIsInitializedCorrectly(checkoutCompletedOn, 1, "Jos", false);

            await When(new OrderCreated(OrderCode, CheckoutType.BankTransfer, 4555, "", DateTime.Now));
            await ThenSagaStateIs(StateMachine.Created);

            var invoicedOn = DateTime.Now;
            //await Bus.Publish(new OrderInvoiced(OrderCode, 123, "123", InvoiceType.NewCustomerOrderProforma, invoicedOn));
            await ThenSagaStateIs(StateMachine.Created);

            await ThenOrderInvoicedEventIsPublished;

            var orderExpired = SubscribeHandler<OrderExpired>(x => x.Message.OrderCode == OrderCode);

            await orderExpired.IsNotCompletedAfter(TimeSpan.FromSeconds(20));

            await Task.Delay(TimeSpan.FromSeconds(40));

            await orderExpired.IsCompletedAfter(TimeSpan.FromSeconds(40));
        }
    }
}
